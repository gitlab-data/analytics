<!-- Company KPIs Pod Iteration Planning: 202x-xx-xx - 202x-xx-xx -->

The purpose of this issue is to help us be prepared for the next iteration. It is meant to be lightweight and helpful, not laborious or wasteful. The intent is for each AE to spend 15 to 30 minutes to complete their part of the issue.

# Work days

How many days do you plan on working next iteration? We want this to be transparent for other team members, plus this number will help you plan how much work you will take on during the iteration. Please factor in public holidays and PTO, and subtract one day for each Triage shift.

| Team member | Working days | Notes (if you are taking PTO, please indicate the days) |
|-------------|--------------|---------------------------------------------------------|
| @chrissharp |              |                                                         |
| @j_kim      |              |                                                         |
| @dantenel   |              |                                                         |
| @annie-analyst |              |                                                         |
| @annapiaseczna |              |                                                         |
| @pempey     |              |    |             
| @csnehansh  |              |                 |    

# Capacity

How many points do you expect to complete next iteration? Use the above section to help you generate an estimate. This does not need to be an exact number, an estimate is fine. (Please use 1.5 issue points per development day for Senior+ team members and 1 issue point per development day for associate / intermediate team members to estimate the planned points. These estimates can vary depending upon the team member and the domain they are working in.)

| Team member | Planned points | Notes |
|-------------|----------------|-------|
| @chrissharp |                |       |
| @j_kim      |                |       |
| @dantenel   |                |       |
| @annie-analyst |                |       |
| @annapiaseczna |                |       |
| @pempey     |                |       |
| @csnehansh  |              |                 |    

# Planned work

### AEs
```glql
display: table
fields: state, title, assignees, weight, labels("Priority::*"), labels("workflow::*"), labels("Champion::*"), updatedAt
limit: 20
query: project = "gitlab-data/analytics" and label = "Pod::Company KPIs" and label = "Team::Analytics Engineering" and iteration = x
```

### DEs
```glql
display: table
fields: state, title, assignees, weight, labels("Priority::*"), labels("workflow::*"), labels("Champion::*"), updatedAt
limit: 20
query: project = "gitlab-data/analytics" and label = "Pod::Company KPIs" and label = "Team::Data Platform" and iteration = x
```

# Personal development

Do you have any personal development time planned in the next iteration? Remember that 5%-10% of our bandwidth is reserved for personal development.

| Team member | Yes/No | Notes |
|-------------|--------|-------|
| @chrissharp |        |       |
| @j_kim      |        |       |
| @dantenel   |        |       |
| @annie-analyst |        |       |
| @annapiaseczna |        |       |
| @pempey     |        |       |
| @csnehansh  |              |                 |    


Please leave any questions, comments, or concerns in the comments.

/label ~"Pod::Company KPIs" ~"Iteration Planning"