Closes

#### List and Describe Code Changes on the system/services <!-- focus on why the changes are being made-->

* `change & why it was made`

#### Steps Taken to Test

* _action items_

#### Code Quality

* [ ] Passed all Python CI checks?
* [ ] Function signatures contain [type hints](https://handbook.gitlab.com/handbook/enterprise-data/platform/python-guide/#type-hints)?
* [ ] Imports follow PEP8 [rules for ordering](https://handbook.gitlab.com/handbook/enterprise-data/platform/python-guide/#import-order) and there are no extra imports
* [ ] [Docstrings](https://handbook.gitlab.com/handbook/enterprise-data/platform/python-guide/#docstrings) are found in every single function
* [ ] Was a [unit test](https://handbook.gitlab.com/handbook/enterprise-data/platform/python-guide/#unit-testing) added for any complex in-code logic?
  - [ ] Yes
  - [ ] No: <!--explain why-->
* [ ] Are [exceptions and errors](https://handbook.gitlab.com/handbook/enterprise-data/platform/python-guide/#exception-handling) handled correctly?
* [ ] Is there any data manipulation that [should be done in SQL instead](https://handbook.gitlab.com/handbook/enterprise-data/platform/python-guide/#when-not-to-use-python)?

#### Security Merge Request Review

**Security Considerations/Areas of Focus**:

- Highlight any security-related or Data handling/processing changes from [this section](https://internal.gitlab.com/handbook/enterprise-data/data-platform-security/secure-code-review-process/#when-to-request-a-security-review)

**Security Review Checklists:**

 - [ ] Dependencies and third-party libraries are up-to-date and secure [Data Engineer or Reviewer]
 - [ ] Thorough security review that ensures code follows the [Secure Coding Practices/Guidelines](https://internal.gitlab.com/handbook/enterprise-data/data-platform-security/secure-coding-guidelines/) [Security Engineer]
 - [ ] Provided Feedback/Guidance to fix identified vulnerability [Security Engineer]

**ETA**:

- [Estimated time for review completion]