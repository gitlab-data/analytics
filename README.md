## Data Team

The [Data Team main handbook page](https://handbook.gitlab.com/handbook/enterprise-data/) is the single source of truth for all of our documentation. 

### Contributing

We welcome contributions and improvements, please see the [contribution guidelines](CONTRIBUTING.md).

### License

This code is distributed under the MIT license, please see the [LICENSE](LICENSE) file.

> **Note:** For more details on our Python coding standards, please refer to [GitLab Python Guide](https://handbook.gitlab.com/handbook/enterprise-data/platform/python-guide/).