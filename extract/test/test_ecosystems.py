"""
Test unit for EcoSystems
"""

from collections import namedtuple
from datetime import datetime
from unittest import mock

import pytest
from extract.ecosystems.src.execute import (
    construct_url,
    get_chunks,
    get_document,
    get_formatted_dates,
    get_formatted_params,
    process_endpoint_results,
    upload_chunks_to_snowflake,
)

ParamConfig = namedtuple(
    "ParamConfig",
    [
        "params_template",
        "formatted_start_date",
        "formatted_end_date",
        "offset",
        "page_size",
    ],
)


@pytest.mark.parametrize(
    "test_input,chunk_size,expected",
    [
        (list(range(10)), 3, [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]),
        (list(range(5)), 2, [[0, 1], [2, 3], [4]]),
        (list(range(3)), 5, [[0, 1, 2]]),
        ([], 3, []),
    ],
)
def test_get_chunks(test_input, chunk_size, expected):
    """Test get_chunks function with various inputs

    Args:
        test_input (list): Input list to be chunked
        chunk_size (int): Size of each chunk
        expected (list): Expected output after chunking
    """
    chunks = list(get_chunks(test_input, chunk_size))
    assert chunks == expected


@pytest.mark.parametrize(
    "param_config,expected",
    [
        (
            ParamConfig(
                params_template="start=%s&end=%s&offset=%s&limit=%s",
                formatted_start_date="2024-01-01",
                formatted_end_date="2024-01-02",
                offset=0,
                page_size=100,
            ),
            "start=2024-01-01&end=2024-01-02&offset=0&limit=100",
        ),
        (
            ParamConfig(
                params_template="start=%s&end=%s&offset=%s&limit=%s",
                formatted_start_date="2023-12-31",
                formatted_end_date="2024-01-01",
                offset=50,
                page_size=200,
            ),
            "start=2023-12-31&end=2024-01-01&offset=50&limit=200",
        ),
        (
            ParamConfig(
                params_template="start=%s&end=%s&offset=%s&limit=%s",
                formatted_start_date="2024-02-29",
                formatted_end_date="2024-03-01",
                offset=1000,
                page_size=50,
            ),
            "start=2024-02-29&end=2024-03-01&offset=1000&limit=50",
        ),
    ],
)
def test_get_formatted_params(param_config, expected):
    """Test get_formatted_params function with various inputs

    Args:
        param_config (ParamConfig): A namedtuple containing:
            - params_template (str): URL parameter template string
            - formatted_start_date (str): Start date in YYYY-MM-DD format
            - formatted_end_date (str): End date in YYYY-MM-DD format
            - offset (int): Pagination offset
            - page_size (int): Number of items per page
        expected (str): Expected formatted parameter string
    """
    result = get_formatted_params(
        params_template=param_config.params_template,
        formatted_start_date=param_config.formatted_start_date,
        formatted_end_date=param_config.formatted_end_date,
        offset=param_config.offset,
        page_size=param_config.page_size,
    )
    assert result == expected


@pytest.mark.parametrize(
    "base,api_path,query_params,expected",
    [
        (
            "https://example.com",
            "/api/v1",
            "param1=value1&param2=value2",
            "https://example.com/api/v1?param1=value1&param2=value2",
        ),
        (
            "https://api.test.com",
            "/v2/data",
            "key=123&format=json",
            "https://api.test.com/v2/data?key=123&format=json",
        ),
        ("http://localhost", "/endpoint", "", "http://localhost/endpoint?"),
    ],
)
def test_construct_url(base, api_path, query_params, expected):
    """Test construct_url function with various inputs

    Args:
        base (str): Base URL of the API
        api_path (str): API endpoint path
        query_params (str): Query parameters string
        expected (str): Expected complete URL
    """
    result = construct_url(base, api_path, query_params)
    assert result == expected


@pytest.mark.parametrize(
    "mock_responses,expected_result",
    [
        (
            [
                {"entries": [{"id": 1}, {"id": 2}]},
                {"entries": [{"id": 3}]},
                {"entries": []},
            ],
            [{"id": 1}, {"id": 2}, {"id": 3}],
        ),
        ([{"entries": []}], []),
        (
            [
                {"entries": [{"id": 1}]},
                {"entries": [{"id": 2}]},
                {"entries": [{"id": 3}]},
                {"entries": []},
            ],
            [{"id": 1}, {"id": 2}, {"id": 3}],
        ),
    ],
)
@mock.patch("extract.ecosystems.src.execute.json_utils")
def test_get_document(mock_json_utils, mock_responses, expected_result):
    """Test get_document function with various response scenarios

    Args:
        mock_json_utils: Mocked json utilities
        mock_responses (list): List of mock API responses
        expected_result (list): Expected processed result
    """
    # Test setup
    endpoint = {"api_url": "/document", "params": "start=%s&end=%s&offset=%s&limit=%s"}
    start_date = "2024-01-01"
    end_date = "2024-01-02"

    # Configure mock
    mock_json_utils.get_response_as_dict.side_effect = mock_responses

    # Execute
    result = get_document(endpoint, start_date, end_date)

    # Assert
    assert result == expected_result
    assert mock_json_utils.get_response_as_dict.call_count == len(mock_responses)


@pytest.mark.parametrize(
    "start_date,end_date,expected",
    [
        (
            datetime(2024, 1, 1),
            datetime(2024, 1, 2),
            {"start": "2024-01-01", "end": "2024-01-02"},
        ),
        (
            datetime(2023, 12, 31),
            datetime(2024, 1, 1),
            {"start": "2023-12-31", "end": "2024-01-01"},
        ),
        (
            datetime(2024, 2, 29),
            datetime(2024, 3, 1),
            {"start": "2024-02-29", "end": "2024-03-01"},
        ),
    ],
)
def test_get_formatted_dates(start_date, end_date, expected):
    """Test get_formatted_dates function with various date inputs

    Args:
        start_date (datetime): Start date
        end_date (datetime): End date
        expected (dict): Expected formatted date strings
    """
    result = get_formatted_dates(start_date, end_date)
    assert result == expected


@pytest.mark.parametrize(
    "test_results,test_table,expected_calls",
    [
        (
            [{"id": 1}, {"id": 2}],  # Non-empty results
            "test_table",
            1,  # Should call upload_chunks_to_snowflake once
        ),
        (
            [],  # Empty results
            "empty_table",
            0,  # Should not call upload_chunks_to_snowflake
        ),
        (
            [{"id": i} for i in range(10000)],  # Large result set
            "large_table",
            1,  # Should still call upload_chunks_to_snowflake once
        ),
    ],
)
@mock.patch("extract.ecosystems.src.execute.upload_chunks_to_snowflake")
def test_process_endpoint_results(
    mock_upload, test_results, test_table, expected_calls
):
    """Test process_endpoint_results function with various scenarios

    Args:
        mock_upload: Mock of upload_chunks_to_snowflake function
        test_results (list): Test data to process
        test_table (str): Name of the target table
        expected_calls (int): Expected number of calls to upload_chunks_to_snowflake
    """
    process_endpoint_results(test_results, test_table)

    assert mock_upload.call_count == expected_calls

    if expected_calls > 0:
        mock_upload.assert_called_with(results=test_results, table=test_table)


@pytest.mark.parametrize(
    "test_input,expected_files,expected_calls",
    [
        (
            [{"id": i} for i in range(7500)],  # Large dataset spanning multiple chunks
            ["ecosystems_1.json", "ecosystems_2.json"],
            2,
        ),
        (
            [{"id": i} for i in range(4000)],  # Single chunk dataset
            ["ecosystems_1.json"],
            1,
        ),
        ([], [], 0),  # Empty dataset
    ],
)
@mock.patch("extract.ecosystems.src.execute.upload_to_snowflake")
def test_upload_chunks_to_snowflake(
    mock_upload, test_input, expected_files, expected_calls
):
    """Test upload_chunks_to_snowflake function with various inputs

    Args:
        mock_upload: Mock of upload_to_snowflake function
        test_input (list): Input data to be uploaded
        expected_files (list): Expected file names to be created
        expected_calls (int): Expected number of calls to upload_to_snowflake
    """
    test_table = "test_table"

    upload_chunks_to_snowflake(test_input, test_table)

    # Verify number of calls
    assert mock_upload.call_count == expected_calls

    # Verify each file upload call
    if expected_calls > 0:
        for file_name in expected_files:
            mock_upload.assert_any_call(file=file_name, table_name=test_table)
