"""
saas_usage_ping.py is responsible for orchestrating:
- usage ping combined metrics (sql + redis)
- usage ping namespace
"""

from airflow import DAG
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow_utils import (
    DATA_IMAGE_3_10,
    clone_repo_cmd,
    gitlab_defaults,
    gitlab_pod_env_vars,
    service_ping_dag_config,
    service_ping_dag_secrets,
    service_ping_default_args,
    service_ping_default_pod_env_vars,
)
from kubernetes_helpers import get_affinity, get_toleration

config = service_ping_dag_config["database_instance_combined_metrics"]
NUMBER_OF_TASKS = config["NUMBER_OF_TASKS"]
pod_env_vars = {**gitlab_pod_env_vars, **service_ping_default_pod_env_vars}

dag = DAG(
    config["DAG_NAME"],
    default_args=service_ping_default_args,
    concurrency=5,
    description=config["DAG_DESCRIPTION"],
    schedule_interval="0 7 * * 1",
    catchup=False,
)

# Instance Level Usage Ping
instance_combined_metrics_cmd = f"""
    {clone_repo_cmd} &&
    cd analytics/extract/saas_usage_ping/ &&
    python3 transform_postgres_to_snowflake.py &&
    python3 usage_ping.py saas_instance_combined_metrics
"""

instance_combined_metrics_ping = KubernetesPodOperator(
    **gitlab_defaults,
    image=DATA_IMAGE_3_10,
    task_id=config["task_name"],
    name=config["task_name"],
    secrets=service_ping_dag_secrets,
    env_vars=pod_env_vars,
    arguments=[instance_combined_metrics_cmd],
    affinity=get_affinity(config["affinity"]),
    tolerations=get_toleration(config["toleration"]),
    dag=dag,
)
