"""
Utils module for MailGun
"""
from dataclasses import dataclass
from os import environ as env
from typing import Dict, List


@dataclass
class Config:
    """
    Configuration class
    """

    api_key: str
    domains: List[str]
    base_url: str
    request_timeout: int
    encoding: str
    schema_name: str = "mailgun"
    table_name: str = "events"
    backfill_hours = 24
    overlapping_hours = 2

    @classmethod
    def from_env(cls):
        """
        Get the values
        """
        return cls(
            api_key=env.get("MAILGUN_API_KEY"),
            domains=[
                "customers.gitlab.com",
                "mg.release.gitlab.net",
                "gitlab.io",
                "users.noreply.gitlab.com",
            ],
            base_url="https://api.mailgun.net/v3",
            request_timeout=120,
            encoding="utf-8",
        )


class JsonUtils:
    """
    Utils class for json transformation and filtering
    """

    def __init__(self):
        pass

    @staticmethod
    def flatten_json(data: Dict) -> Dict:
        """Flatten nested JSON structure into a single level dictionary."""
        flattened = {}

        def flatten(x, name: str = "") -> None:
            if isinstance(x, dict):
                for key in x:
                    flatten(x[key], f"{name}{key}_")
            elif isinstance(x, list):
                for i, item in enumerate(x):
                    flatten(item, f"{name}{i}_")
            else:
                flattened[name[:-1]] = x

        flatten(data)
        return flattened

    @staticmethod
    def filter_columns(data: Dict) -> Dict:
        """
        Filter only needed columns
        :param data:
        :return:
        """
        column_filter: List[str] = [
            "domain",
            "id",
            "envelope_sender",
            "envelope_targets",
            "primary-dkim",
            "message_headers_from",
            "message_headers_message-id",
            "message_headers_to",
            "event",
            "recipient-domain",
            "recipient-provider",
            "log-level",
            "recipient",
            "delivery-status_code",
            "delivery-status_mx-host",
            "delivery-status_description",
            "delivery-status_attempt-no",
            "delivery-status_message",
            "timestamp",
            "campaigns",
            "reason",
            "delivery-status_bounce-code",
            "delivery-status_bounce-type",
            "geolocation_region",
            "geolocation_city",
            "geolocation_timezone",
            "geolocation_country",
        ]

        return {k: v for k, v in data.items() if k in column_filter}

    @staticmethod
    def filter_rows_per_subject(data: dict) -> bool:
        """
        Filter only needed rows
        :param data:
        :return:
        """
        exclude_message_headers_subject = {
            "Managing users in your subscription",
            "Additional charges for your GitLab subscription",
            "Your GitLab subscription has been reconciled",
        }

        return data.get("message_headers_subject") in exclude_message_headers_subject

    @staticmethod
    def enrich_json(data: dict, enrichment_data: tuple) -> dict:
        """
        Enriches JSON data with additional fields from enrichment data tuple.

        Args:
            data: Original dictionary to enrich
            enrichment_data: Tuple containing key-value pair to add to the data
                in format (field_name, field_value)

        Returns:
            Dictionary with added enrichment fields
        """
        if not isinstance(data, dict):
            raise TypeError("Input data must be a dictionary")

        if not isinstance(enrichment_data, tuple) or len(enrichment_data) != 2:
            raise ValueError("Enrichment data must be a 2-element tuple")

        enriched_data = data.copy()
        field_name, field_value = enrichment_data
        enriched_data[field_name] = field_value

        return enriched_data
