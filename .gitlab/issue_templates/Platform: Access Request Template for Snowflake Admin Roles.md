<!---
  Use this template to request elevated roles in Snowflake before submitting an Access Request (AR). Once the template is completed and approved, an AR can be initiated, which will still require manager approval.
--->

## Requestor Details:

GitLab handle: [Your GitLab handle]

## Access Details:

Role Requested: [Specify the role: Account Admin, Security Admin, or Sys Admin]

Purpose of Access: [Brief description of why access is required, e.g., "To perform administrative tasks related to database management and configuration."]

Duration of Access: [Specify duration, e.g., "Ongoing" or "From [Start Date] to [End Date]"]

## Approval Details:

Approval:

GitLab Handle: [Approving Admin's GitLab handle]

_Note: Any of the existing admins with access to the requested role (Account Admin, Security Admin, or Sys Admin) can approve the request based on the purpose provided._

## For Internal Use Only

Request Processed By: [Admin Name]

Date Processed: [Date]

Access Granted On: [Date]

Remarks: [Optional]