"""
Extracts data from the EcoSystems API event stream
"""
import csv
import datetime
import json
import sys
from logging import basicConfig, info
from os import environ as env
from typing import List

import pandas as pd
from dateutil import parser as date_parser
from ecosystems_utils import Config, JsonUtils
from fire import Fire
from gitlabdata.orchestration_utils import (
    snowflake_engine_factory,
    snowflake_stage_load_copy_remove,
)

config_dict = env.copy()
config = Config()
json_utils = JsonUtils()


def get_chunks(seq: List, size: int):
    """
    Splits a list into chunks of a given size.
    :param seq:
    :param size:
    :return:
    """
    return (seq[pos : pos + size] for pos in range(0, len(seq), size))


def get_formatted_params(
    params_template,
    formatted_start_date,
    formatted_end_date,
    offset: int,
    page_size: int,
) -> str:
    """
    Formats the query parameters with pagination values
    """

    return params_template % (
        formatted_start_date,
        formatted_end_date,
        offset,
        page_size,
    )


def construct_url(base: str, api_path, query_params: str) -> str:
    """
    Constructs the full API URL with parameters
    """
    base_url = f"{base}{api_path}"
    return f"{base_url}?{query_params}"


def get_document(endpoint: dict, formatted_start_date, formatted_end_date) -> list:
    """
    Get document data from the API with pagination support

    Args:
        endpoint (dict): Dictionary containing API endpoint configuration
        formatted_start_date (str): Start date in YYYY-MM-DD format
        formatted_end_date (str): End date in YYYY-MM-DD format

    Returns:
        list: List of flattened JSON responses from the API
    """

    res = []
    current_offset = 0
    page_size = 100
    loop_no = 0
    while True:
        # Construct API URL with pagination parameters
        loop_no += 1

        params = get_formatted_params(
            params_template=endpoint.get("params"),
            formatted_start_date=formatted_start_date,
            formatted_end_date=formatted_end_date,
            offset=current_offset,
            page_size=page_size,
        )

        url = construct_url(
            base=config.base_url, api_path=endpoint["api_url"], query_params=params
        )
        response = json_utils.get_response_as_dict(url=url)

        if response.get("entries", None):
            res.extend(response.get("entries"))
            current_offset = loop_no * page_size + 1
        else:
            info("No more responses for the document API")
            break

    return res


def get_cva(endpoint: dict) -> list:
    """
    Get CVA report data from the API and convert it to a list of dictionaries

    Args:
        endpoint (dict): Dictionary containing API endpoint configuration

    Returns:
        list: List of dictionaries containing CVA report data, filtered to remove entries with null Document ID
    """
    # Construct full URL from base URL and endpoint
    base_url = config.base_url
    api_path = endpoint["api_url"]
    url = f"{base_url}{api_path}"

    response = json_utils.get_response(url=url).text
    reader = csv.reader(response.split("\n"), delimiter=",")

    list_reader = list(reader)
    df = pd.DataFrame(list_reader[1:], columns=list_reader[0])

    return [x for x in df.to_dict(orient="records") if x["Document ID"] is not None]


def upload_chunks_to_snowflake(results: list, table: str):
    """

    :param results:
    :return:
    """

    file_count = 0
    info("... Start creating chunks")
    for group in get_chunks(results, 5000):
        file_count = file_count + 1
        file_name = f"ecosystems_{file_count}.json"

        with open(file_name, mode="w", encoding=config.encoding) as outfile:
            json.dump(group, outfile)

        upload_to_snowflake(file=file_name, table_name=table)
    info("... End creating chunks")


def process_endpoint_results(results: list, table_name: str) -> None:
    """
    Process and upload results to Snowflake if results exist

    Args:
        results (list): List of results to process
        table_name (str): Name of the table to upload to
    """
    if results:
        upload_chunks_to_snowflake(results=results, table=table_name)
    else:
        info(f"No results found for table: {table_name}")


def get_formatted_dates(
    start_date: datetime.datetime, end_date: datetime.datetime
) -> dict:
    """
    Format dates for API requests

    Args:
        start_date (datetime.datetime): Start date to format
        end_date (datetime.datetime): End date to format

    Returns:
        dict: Dictionary containing formatted start and end dates
    """
    return {
        "start": start_date.strftime("%Y-%m-%d"),
        "end": end_date.strftime("%Y-%m-%d"),
    }


def extract_endpoint_data(
    start_date: datetime.datetime, end_date: datetime.datetime
) -> None:
    """
    Get the data from the API and load it into Snowflake for each configured endpoint.

    Args:
        start_date (datetime.datetime): Start date for data extraction
        end_date (datetime.datetime): End date for data extraction
    """
    formatted_dates = get_formatted_dates(start_date, end_date)

    # Define handlers for different endpoint types
    endpoint_handlers = {
        "document": lambda ep: get_document(
            endpoint=ep,
            formatted_start_date=formatted_dates["start"],
            formatted_end_date=formatted_dates["end"],
        ),
        "cva": get_cva,
    }

    for endpoint in config.endpoints:
        endpoint_name = endpoint.get("name")
        table_name = endpoint.get("table_name")

        info(f"Processing endpoint: {endpoint_name}")

        # Get handler function for endpoint type or return empty list if not found
        handler = endpoint_handlers.get(endpoint_name, lambda _: [])
        results = handler(endpoint)

        process_endpoint_results(results, table_name)


def upload_to_snowflake(file: str, table_name: str) -> None:
    """
    Upload data to Snowflake
    """
    info("START: Uploading data to Snowflake")
    snowflake_engine = snowflake_engine_factory(config_dict, "LOADER")

    snowflake_stage_load_copy_remove(
        file=file,
        stage=f"{config.schema_name}.ECOSYSTEMS_STAGE",
        table_path=f"{config.schema_name}.{table_name}",
        engine=snowflake_engine,
        on_error="ABORT_STATEMENT",
    )
    info("END: Uploading data to Snowflake")


def load_endpoints():
    """
    CLI main function, starting point for setting up engine and processing data.
    """

    # This extends the time window to handle late processing on the API.

    end_date = date_parser.parse(config_dict["START_TIME"]) - datetime.timedelta(
        hours=config.overlapping_hours
    )
    start_date = end_date - datetime.timedelta(hours=config.backfill_hours)

    info(
        f"Running from {start_date.strftime('%Y-%m-%d')} to {end_date.strftime('%Y-%m-%d')}"
    )

    extract_endpoint_data(start_date=start_date, end_date=end_date)


if __name__ == "__main__":
    basicConfig(stream=sys.stdout, level=20)
    info("Start.")
    Fire(load_endpoints)
    info("Complete.")
