"""
Unit to generate backfill data for namespace
Need a parameter in json ie:
Name: NAMESPACE_BACKFILL_VAR
Content:
{"start_date": "2022-10-01",
 "end_date": "2022-10-25",
 "metrics_backfill": ['metric_x_last_28_days','some_other_metrics','3rd.metrics'] # this is passed as a list of metrics
}
"""

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.models import Variable
from airflow_utils import (
    generate_backfill_task,
    get_date,
    get_monday,
    get_run_dates,
    service_ping_dag_config,
    service_ping_default_args,
)

metrics_type = "database_namespace_metrics_backfill"
config = service_ping_dag_config[metrics_type]

BACKFILL_PARAMETERS = Variable.get(
    "NAMESPACE_BACKFILL_VAR", deserialize_json=True, default_var=None
)

if BACKFILL_PARAMETERS:
    start_date = get_date(parameters=BACKFILL_PARAMETERS, date_param="start_date")
    start_date = get_monday(day=start_date)

    end_date = get_date(parameters=BACKFILL_PARAMETERS, date_param="end_date")
    metrics = BACKFILL_PARAMETERS.get("metrics_backfill")

    dag = DAG(
        config["DAG_NAME"],
        default_args=service_ping_default_args,
        schedule_interval=None,
        concurrency=2,
        description=config["DAG_DESCRIPTION"],
        catchup=False,
    )

    dummy_start = DummyOperator(task_id=f"start_{config['task_name']}", dag=dag)

    backfill_tasks = []
    for run in get_run_dates(start=start_date, end=end_date):
        dummy_start >> generate_backfill_task(
            dag=dag, metrics_type=metrics_type, metrics=metrics, run_date=run
        )
