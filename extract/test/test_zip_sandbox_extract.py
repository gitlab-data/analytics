"""
Test for Zip sandbox pipeline
"""
import os
from datetime import datetime, timezone
from unittest.mock import MagicMock, patch

import pytest
from zip_sandbox_utils import (
    extract_json_and_next_page_token,
    get_start_and_end_date,
    upload_events_to_snowflake,
    upload_payload_to_snowflake,
)


@pytest.fixture
def mock_env():
    """
    Mock environment variables.
    """
    return {
        "is_full_refresh": "false",
        "data_interval_start": "2023-02-01T00:00:00+00:00",
        "data_interval_end": "2023-02-02T00:00:00+00:00",
        "ZIP_SANDBOX_API_KEY": "test_api_key",
    }


# pylint: disable=redefined-outer-name
def test_get_start_and_end_date(mock_env):
    """
    Test get_start_and_end_date function.
    """
    with patch.dict(os.environ, mock_env):
        start, end = get_start_and_end_date()
        assert start == int(datetime(2023, 2, 1, tzinfo=timezone.utc).timestamp())
        assert end == int(datetime(2023, 2, 2, tzinfo=timezone.utc).timestamp())


def test_get_start_and_end_date_full_refresh(mock_env):
    """
    Test get_start_and_end_date function.
    """
    mock_env["is_full_refresh"] = "true"
    with patch.dict(os.environ, mock_env):
        start, end = get_start_and_end_date()
        assert start == int(datetime(2023, 2, 1, tzinfo=timezone.utc).timestamp())
        assert end == int(datetime(2023, 2, 2, tzinfo=timezone.utc).timestamp())


# pylint: enable=redefined-outer-name


@patch("zip_sandbox_utils.upload_payload_to_snowflake")
def test_upload_events_to_snowflake(mock_upload):
    """
    Test upload_events_to_snowflake function.
    """
    events = [{"id": 1}, {"id": 2}]
    upload_events_to_snowflake(events, "requests")
    mock_upload.assert_called_once_with(
        {"data": events},
        "zip_sandbox",
        "zip_load_stage",
        "requests",
        "zip_sandbox_requests.json",
    )


@patch("zip_sandbox_utils.snowflake_engine_factory")
@patch("zip_sandbox_utils.snowflake_stage_load_copy_remove")
@patch("json.dump")
def test_upload_payload_to_snowflake(
    mock_json_dump, mock_snowflake_stage, mock_engine_factory
):
    """
    Test upload_payload_to_snowflake function.
    """
    mock_engine = MagicMock()
    mock_engine_factory.return_value = mock_engine

    payload = {"data": [{"id": 1}]}
    upload_payload_to_snowflake(
        payload, "test_schema", "test_stage", "requests", "test.json"
    )

    mock_json_dump.assert_called_once()
    mock_snowflake_stage.assert_called_once_with(
        "test.json", "test_schema.test_stage", "test_schema.requests", mock_engine
    )
    mock_engine.dispose.assert_called_once()


def test_extract_json_and_next_page_token():
    """
    Test extract_json_and_next_page_token function.
    """
    mock_response = MagicMock()
    mock_response.json.return_value = {
        "list": [{"id": 1}, {"id": 2}],
        "next_page_token": "next_token",
    }

    response_list, next_page_token = extract_json_and_next_page_token(mock_response)

    assert response_list == [{"id": 1}, {"id": 2}]
    assert next_page_token == "next_token"


# pylint: disable=redefined-outer-name
def test_get_start_and_end_date_invalid_date_format(mock_env):
    """
    Test get_start_and_end_date function.
    """
    mock_env["data_interval_start"] = "invalid_date_format"
    with patch.dict(os.environ, mock_env):
        with pytest.raises(ValueError):
            get_start_and_end_date()


# pylint: enable=redefined-outer-name
