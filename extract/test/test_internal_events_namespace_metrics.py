"""
Test internal namespace metrics
"""

import os
from os import environ

import pytest
import yaml
from extract.saas_usage_ping.internal_events_namespace_metrics import (
    InternalEventsNamespaceMetrics,
    SQLGenerate,
)

table_name_prefix = "mart_behavior_structured_event_service_ping_metrics"


@pytest.fixture(name="internal_events_namespace_metrics")
def get_internal_events_namespace_metrics():
    """
    Fixture to create an instance of InternalNamespaceMetrics
    """
    environ["SNOWFLAKE_PROD_DATABASE"] = "PROD"
    return InternalEventsNamespaceMetrics()


@pytest.fixture(name="internal_events_namespace_metrics_absolute_path")
def fixture_setup_metrics_file_path(internal_events_namespace_metrics):
    """Set up the correct path to the metrics YAML file."""
    # Set the correct path
    project_root = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
    absolute_file_path = os.path.join(
        project_root,
        "saas_usage_ping",
        internal_events_namespace_metrics.util.INTERNAL_EVENTS_METRICS_TO_KEEP_FILE,
    )

    # Override with absolute path
    internal_events_namespace_metrics.util.INTERNAL_EVENTS_METRICS_TO_KEEP_FILE = (
        absolute_file_path
    )

    return internal_events_namespace_metrics


@pytest.fixture(name="sql_generate")
def get_sql_generate():
    """Fixture to create an instance of SQLGenerate"""
    environ["SNOWFLAKE_PROD_DATABASE"] = "PROD"
    return SQLGenerate()


@pytest.mark.parametrize(
    "attribute, expected_value",
    [
        ("database", "PROD"),
        ("schema", "common_mart_product"),
    ],
)
def test_initialization(sql_generate, attribute, expected_value):
    """
    Test class creation attributes
    """
    assert getattr(sql_generate, attribute) == expected_value


@pytest.mark.parametrize(
    "time_frame, expected_value",
    [
        (
            "7d",
            "AND behavior_at BETWEEN DATEADD(DAY, -7, between_end_date) AND between_end_date ",
        ),
        (
            "28d",
            "AND behavior_at BETWEEN DATEADD(DAY, -28, between_end_date) AND between_end_date ",
        ),
        ("1d", ""),
    ],
)
def test_sql_generate_get_time_frame(sql_generate, time_frame, expected_value):
    """Test the get_time_frame method of SQLGenerate class"""

    assert sql_generate.get_time_frame(time_frame=time_frame) == expected_value


def test_sql_generate_get_event_list(sql_generate):
    """
    Test the get_event_list method of SQLGenerate class
    """
    metrics = {
        "events": [{"name": "event1"}, {"name": "event2"}],
        "options": {"events": ["option1", "option2"]},
    }
    expected = "AND redis_event_name IN ('event1','event2','option1','option2') "
    assert sql_generate.get_event_list(metrics) == expected

    metrics_single = {"events": [{"name": "single_event"}]}
    expected_single = "AND redis_event_name = 'single_event' "
    assert sql_generate.get_event_list(metrics_single) == expected_single


@pytest.mark.parametrize(
    "metrics, result",
    [
        (
            {
                "data_category": "optional",
                "key_path": "redis_hll_counters.analytics.g_analytics_contribution_monthly",
                "status": "active",
                "time_frame": "28d",
                "data_source": "redis_hll",
                "options": {"events": ["g_analytics_contribution"]},
            },
            "AND redis_event_name = 'g_analytics_contribution' ",
        ),
        (
            {
                "data_category": "optional",
                "key_path": "redis_hll_counters.analytics.g_analytics_contribution_monthly",
                "status": "active",
                "time_frame": "28d",
                "data_source": "redis_hll",
                "options": {"events": []},
            },
            "",
        ),
        (
            {
                "data_category": "optional",
                "key_path": "redis_hll_counters.analytics.g_analytics_contribution_monthly",
                "status": "active",
                "time_frame": "28d",
                "data_source": "redis_hll",
                "options": {},
            },
            "",
        ),
        (
            {
                "data_category": "optional",
                "key_path": "redis_hll_counters.analytics.g_analytics_contribution_monthly",
                "status": "active",
                "time_frame": "28d",
                "data_source": "redis_hll",
            },
            "",
        ),
    ],
)
def test_sql_generate_no_events_no_details(sql_generate, metrics, result):
    """
    Test the get_event_list method of SQLGenerate class
    where no events and no options are provided or combination
    """

    assert sql_generate.get_event_list(metrics) == result


@pytest.mark.parametrize(
    "metrics, result",
    [
        (
            {
                "data_source": "redis",
                "key_path": "***********",
                "events": [{"name": "event1"}],
                "time_frame": "7d",
            },
            (
                "SELECT ultimate_parent_namespace_id AS id, "
                "ultimate_parent_namespace_id AS namespace_ultimate_parent_id, "
                "COUNT(1) AS counter_value "
                f"FROM database_placeholder.schema_placeholder.{table_name_prefix}_7d "
                "WHERE metrics_path = '***********' "
                "AND redis_event_name = 'event1' "
                "AND behavior_at BETWEEN DATEADD(DAY, -7, between_end_date) AND between_end_date "
                "GROUP BY ALL;"
            ),
        ),
        (
            {
                "data_source": "redis_hll",
                "key_path": "***********",
                "events": [{"name": "event1"}],
                "time_frame": "7d",
            },
            (
                "SELECT ultimate_parent_namespace_id AS id, "
                "ultimate_parent_namespace_id AS namespace_ultimate_parent_id, "
                "COUNT(DISTINCT gsc_pseudonymized_user_id) AS counter_value "
                f"FROM database_placeholder.schema_placeholder.{table_name_prefix}_7d "
                "WHERE metrics_path = '***********' "
                "AND redis_event_name = 'event1' "
                "AND behavior_at BETWEEN DATEADD(DAY, -7, between_end_date) AND between_end_date "
                "GROUP BY ALL;"
            ),
        ),
        (
            {
                "data_source": "redis_hll",
                "key_path": "***********",
                "events": [{"name": "event1"}],
                "time_frame": "28d",
            },
            (
                "SELECT ultimate_parent_namespace_id AS id, "
                "ultimate_parent_namespace_id AS namespace_ultimate_parent_id, "
                "COUNT(DISTINCT gsc_pseudonymized_user_id) AS counter_value "
                f"FROM database_placeholder.schema_placeholder.{table_name_prefix}_28d "
                "WHERE metrics_path = '***********' "
                "AND redis_event_name = 'event1' "
                "AND behavior_at BETWEEN DATEADD(DAY, -28, between_end_date) AND between_end_date "
                "GROUP BY ALL;"
            ),
        ),
    ],
)
def test_sql_generate_transform(sql_generate, metrics, result):
    """
    Test the transform method of SQLGenerate class
    """
    result_replaced = result.replace("database_placeholder", sql_generate.database)
    result_replaced = result_replaced.replace("schema_placeholder", sql_generate.schema)

    assert sql_generate.transform(metrics=metrics) == result_replaced


@pytest.mark.parametrize(
    "metrics, expected",
    [
        (
            {
                "key_path": "test.metric",
                "events": [{"name": "event1"}],
                "time_frame": "7d",
            },
            ["test.metric", "SELECT", True, "namespace", "7d"],
        ),
        (
            {
                "key_path": "test.metric",
                "events": [{"name": "event1"}],
                "time_frame": "all",
            },
            ["test.metric", "SELECT", False, "namespace", "all"],
        ),
    ],
)
def test_internal_events_namespace_metrics_get_sql_metrics_definition(
    internal_events_namespace_metrics, metrics, expected
):
    """
    Test the get_sql_metrics_definition method of InternalNamespaceMetrics class
    """

    result = internal_events_namespace_metrics.get_sql_metrics_definition(metrics)
    assert result["counter_name"] == expected[0]
    assert expected[1] in result["counter_query"]
    assert result["time_window_query"] is expected[2]
    assert result["level"] == expected[3]
    assert result["time_frame"] == expected[4]


def test_internal_events_namespace_metrics_generate_sql_metrics(
    internal_events_namespace_metrics,
):
    """Test the generate_sql_metrics method of InternalNamespaceMetrics class"""
    metrics_data = [
        {"key_path": "metric1", "status": "active"},
        {"key_path": "metric2", "status": "inactive"},
        {"key_path": "metric3", "status": "active"},
    ]
    result = internal_events_namespace_metrics.generate_sql_metrics(metrics_data)
    assert len(result) == 3
    assert result[0]["counter_name"] == "metric1"
    assert result[1]["counter_name"] == "metric2"
    assert result[2]["counter_name"] == "metric3"


def test_analytics_g_analytics_contribution_monthly(sql_generate):
    """
    the filter should be AND redis_event_name = 'g_analytics_contribution'
    """

    metrics = {
        "data_category": "optional",
        "key_path": "redis_hll_counters.analytics.g_analytics_contribution_monthly",
        "description": "Unique visitors to /groups/:group/-/contribution_analytics by month",
        "product_group": "optimize",
        "value_type": "number",
        "status": "active",
        "time_frame": "28d",
        "data_source": "redis_hll",
        "instrumentation_class": "RedisHLLMetric",
        "options": {"events": ["g_analytics_contribution"]},
        "distribution": ["ee"],
        "tier": ["premium", "ultimate"],
        "performance_indicator_type": [],
        "milestone": "<13.9",
        "introduced_by_url": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/54332",
    }

    result = sql_generate.transform(metrics=metrics)
    assert "redis_event_name = 'g_analytics_contribution'" in result
    assert "redis_event_name = 'events'" not in result


@pytest.mark.parametrize(
    "metrics_definition, expected_value",
    [
        ({"data_source": "redis"}, "COUNT(1)"),
        ({"data_source": "redis_hll"}, "COUNT(DISTINCT gsc_pseudonymized_user_id)"),
        (
            {"data_source": "internal_events", "events": [{"name": "event_name"}]},
            "COUNT(1)",
        ),
        (
            {
                "data_source": "internal_events",
                "events": [{"name": "event_name", "unique": "user.id"}],
            },
            "COUNT(DISTINCT gsc_pseudonymized_user_id)",
        ),
        ({"data_source": "unknown"}, "COUNT(1)"),
        ({"data_source": "database"}, "COUNT(1)"),
        ({"data_source": "internal_events"}, "COUNT(1)"),
    ],
)
def test_determine_count_type(sql_generate, metrics_definition, expected_value):
    """
    Test determine_count_type method for various
    """

    assert sql_generate.determine_count_type(metrics_definition) == expected_value


def test_analytics_web_ide_pipelines(sql_generate):
    """
    the filter should be AND redis_event_name = 'g_analytics_contribution'
    """

    metrics = {
        "data_category": "optional",
        "key_path": "counts.web_ide_pipelines",
        "description": "Count of Pipeline tab views in the Web IDE",
        "product_group": "remote_development",
        "value_type": "number",
        "status": "active",
        "time_frame": "all",
        "data_source": "redis",
        "instrumentation_class": "RedisMetric",
        "options": {
            "prefix": "web_ide",
            "event": "pipelines_count",
            "include_usage_prefix": False,
        },
        "distribution": ["ce", "ee"],
        "tier": ["free", "premium", "ultimate"],
        "performance_indicator_type": [],
        "milestone": "<13.9",
        "introduced_by_url": "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/54332",
    }

    result = sql_generate.transform(metrics=metrics)

    assert "COUNT(1)" in result
    assert "counts.web_ide_pipelines" in result


def test_filter_metrics(internal_events_namespace_metrics_absolute_path):
    """
    Test the filter_metrics method of InternalNamespaceMetrics class
    """
    existing_metric = "analytics_unique_visits.i_analytics_dev_ops_adoption"
    test_metrics = [
        {
            "data_source": "redis",
            "status": "active",
            "key_path": existing_metric,
        },
        {
            "data_source": "redis_hll",
            "status": "active",
            "key_path": existing_metric,
        },
        {
            "data_source": "internal_events",
            "status": "active",
            "key_path": existing_metric,
        },
        {
            "data_source": "some_invalid_data_source",
            "status": "active",
            "key_path": existing_metric,
        },
        {
            "data_source": "redis",
            "status": "inactive",
            "key_path": existing_metric,
        },
        {
            "data_source": "redis_hll",
            "status": "deprecated",
            "key_path": existing_metric,
        },
        {
            "data_source": "internal_events",
            "status": "active",
            "key_path": "some_invalid_metric",
        },
    ]

    filtered_metrics = internal_events_namespace_metrics_absolute_path.filter_metrics(
        test_metrics
    )

    assert len(filtered_metrics) == 3
    assert all(
        metric["data_source"]
        in internal_events_namespace_metrics_absolute_path.data_source
        for metric in filtered_metrics
    )
    assert all(
        metric["status"]
        in internal_events_namespace_metrics_absolute_path.metrics_status
        for metric in filtered_metrics
    )


@pytest.mark.parametrize(
    "time_frame, expected_table",
    [
        (
            "7d",
            f"{table_name_prefix}_7d",
        ),
        (
            "28d",
            f"{table_name_prefix}_28d",
        ),
        (
            "all",
            f"{table_name_prefix}",
        ),
        (
            "backfill",
            f"{table_name_prefix}",
        ),
        ("invalid", ""),
    ],
)
def test_sql_generate_get_table_name(sql_generate, time_frame, expected_table):
    """
    Test the get_table_name method of SQLGenerate class
    """
    assert (
        sql_generate.get_table_name(time_frame)
        == f"{sql_generate.database}.{sql_generate.schema}.{expected_table}"
    )


@pytest.mark.parametrize(
    "metrics",
    [
        {
            "key_path": "counts.web_ide_pipelines",
            "status": "active",
            "time_frame": "7d",
            "data_source": "redis",
        },
        {
            "key_path": "counts.web_ide_pipelines",
            "status": "active",
            "time_frame": "28d",
            "data_source": "redis",
        },
        {
            "key_path": "counts.web_ide_pipelines",
            "status": "active",
            "time_frame": "all",
            "data_source": "redis",
        },
    ],
)
def test_sql_generate_transform_backfill(sql_generate, metrics):
    """
    Test the transform method of SQLGenerate class

    When time_frame = all, it should return the same sql statement
    regardless if its backfill or not.

    Else if tiemframe=[7d,28d], the sql statement should be different,
    depending on if it's a backfill or not
    """

    sql_backfill = SQLGenerate(is_backfill=True)

    no_backfill_sql_statement = sql_generate.transform(metrics=metrics)
    backfill_sql_statement = sql_backfill.transform(metrics=metrics)

    if metrics.get("time_frame") == "all":
        assert no_backfill_sql_statement == backfill_sql_statement
        assert (
            f"FROM {sql_generate.database}.{sql_generate.schema}.{table_name_prefix} "
            in backfill_sql_statement
        )
        assert (
            f"FROM {sql_generate.database}.{sql_generate.schema}.{table_name_prefix}_"
            not in backfill_sql_statement
        )
    else:
        assert no_backfill_sql_statement != backfill_sql_statement
        assert (
            f"FROM {sql_generate.database}.{sql_generate.schema}.{table_name_prefix}_"
            in no_backfill_sql_statement
        )
        assert (
            f"FROM {sql_generate.database}.{sql_generate.schema}.{table_name_prefix} "
            not in no_backfill_sql_statement
        )


@pytest.mark.parametrize(
    "metrics, is_backfill, expected_result",
    [
        (
            {"data_source": "redis", "time_frame": "7d"},
            False,
            "",
        ),
        (
            {"data_source": "redis_hll", "time_frame": "28d"},
            False,
            "",
        ),
        (
            {"data_source": "internal_events", "time_frame": "all"},
            False,
            "AND data_source='internal_events' AND time_frame='all' ",
        ),
        (
            {"data_source": "redis", "time_frame": "7d"},
            True,
            "AND data_source='redis' AND time_frame='7d' ",
        ),
        (
            {"data_source": "redis_hll", "time_frame": "28d"},
            True,
            "AND data_source='redis_hll' AND time_frame='28d' ",
        ),
        (
            {"data_source": "internal_events", "time_frame": "all"},
            True,
            "AND data_source='internal_events' AND time_frame='all' ",
        ),
    ],
)
def test_get_additional_filters(sql_generate, metrics, is_backfill, expected_result):
    """
    Test the get_additional_filters method of SQLGenerate class
    """
    sql_generate.is_backfill = is_backfill

    assert sql_generate.get_additional_filters(metrics) == expected_result


@pytest.mark.skipif(
    "RUNALL" not in environ,
    reason="Takes too long if run in the pipeline, want to run locally only",
)
def test_validate_metrics_transformed_file(
    internal_events_namespace_metrics_absolute_path,
):
    """
    Validate transformed file
    if you want to run it locally, execute the command:
        export RUNALL=YES
    Check variable:
        echo $RUNALL
    """
    metrics_raw = internal_events_namespace_metrics_absolute_path.util.get_response(
        url=internal_events_namespace_metrics_absolute_path.url
    )
    metrics_prepared = yaml.safe_load(metrics_raw.text)

    metrics_prepared = internal_events_namespace_metrics_absolute_path.filter_metrics(
        metrics=metrics_prepared
    )

    internal_events_namespace_metrics_absolute_path.is_backfill = True
    metrics_prepared = (
        internal_events_namespace_metrics_absolute_path.generate_sql_metrics(
            metrics_data=metrics_prepared
        )
    )

    def validate_metric(metric):
        assert metric["time_window_query"] in [False, True]
        assert metric["time_frame"] in ["all", "7d", "28d"]
        assert metric["level"] == "namespace"
        assert (
            metric["time_frame"] == "all"
            and not metric["time_window_query"]
            or metric["time_frame"] != "all"
            and metric["time_window_query"]
        )
        assert "SELECT" in metric["counter_query"]
        assert "FROM" in metric["counter_query"]
        assert "GROUP BY" in metric["counter_query"]
        assert "WHERE" in metric["counter_query"]

    for metric in metrics_prepared:
        validate_metric(metric)


def test_get_metric_names_to_keep(internal_events_namespace_metrics_absolute_path):
    metric_names = (
        internal_events_namespace_metrics_absolute_path.get_metric_names_to_keep()
    )

    # Basic assertions
    assert metric_names is not None
    assert isinstance(metric_names, list)
    assert len(metric_names) > 0, "Expected at least some metrics to be defined"

    # check if expected_metric exists in yaml file
    expected_metrics = ["counts.count_total_create_ci_build_monthly"]
    for metric in expected_metrics:
        assert (
            metric in metric_names
        ), f"Expected metric '{metric}' not found in results"
