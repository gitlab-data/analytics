"""
Unit to test build table name
"""
from datetime import datetime
from unittest.mock import patch

import pytest
from main import build_table_name, create_table_name


@pytest.mark.parametrize(
    "table_prefix, table_name, table_suffix, expected",
    [
        ("prefix_", "table", "_suffix", "prefix_table_suffix"),
        (None, "table", None, "table"),
        ("prefix_", "table", None, "prefix_table"),
        (None, "table", "_suffix", "table_suffix"),
        ("pre_", "my_table", "_post", "pre_my_table_post"),
    ],
)
def test_build_table_name_parametrized(
    table_prefix, table_name, table_suffix, expected
):
    """
    Test build_table_name_parametrized
    """
    assert build_table_name(table_prefix, table_name, table_suffix) == expected


@pytest.mark.parametrize(
    "table_prefix, table_name, expected_backup, expected_original",
    [
        ("prefix_", "table", "prefix_table_20230501", "prefix_table"),
        ("", "test_table", "test_table_20230501", "test_table"),
        ("bkp_", "data", "bkp_data_20230501", "bkp_data"),
    ],
)
def test_create_table_name(
    table_prefix, table_name, expected_backup, expected_original
):
    """
    Test create_table_name
    """
    with patch("main.datetime") as mock_datetime:
        mock_datetime.now.return_value = datetime(2023, 5, 1)
        backup_name, original_name = create_table_name(table_prefix, table_name)

        assert backup_name == expected_backup
        assert original_name == expected_original
