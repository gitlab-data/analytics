""" Test deprovision_users.py """

from argparse import Namespace
from unittest.mock import patch

import pytest
from deprovision_users import (
    process_args,
    flatten_list_of_tuples,
    compare_users,
    validate_users_and_roles,
)

KEY_ROLES = ["sysadmin", "accountadmin", "securityadmin"]
ROLE_MULTIPLER = 1000


@patch("deprovision_users.parse_arguments")
def test_process_args(mock_parse_arguments):
    """
    Test that args are processed correctly
    """
    users_to_remove = ["usertoadd1", "usertoadd2"]
    test_run = True
    mock_parse_arguments.return_value = Namespace(
        users_to_remove=users_to_remove,
        test_run=test_run,
    )

    parsed_args = process_args()
    assert parsed_args[0] == users_to_remove
    assert parsed_args[1] is test_run


@pytest.mark.parametrize(
    "test_tuple_list, expected_list",
    [
        ([(1,)], [1]),
        ([(2,)], [2]),
        ([(1, 2)], [1]),
    ],
)
def test_flatten_list_of_tuples(test_tuple_list, expected_list):
    """
    Test that each tuple's first element is flattened into a list
    """
    assert flatten_list_of_tuples(test_tuple_list) == expected_list


@pytest.mark.parametrize(
    "users_roles_yaml, users_snowflake, missing_users_in_roles",
    [
        ([], ["user1", "user2"], ["user1", "user2"]),
        (["user1", "user2"], ["user1", "user2"], []),
        (["user1", "user2"], [], []),
    ],
)
def test_compare_users(users_roles_yaml, users_snowflake, missing_users_in_roles):
    """
    Check that users_snowflake that are misisng in users_roles_yaml are returned
    Scenarios:
        1. two users in users_snowflake, none in roles_yaml, both are missing usres
        2. Two users in both users_snowflake and roles_yaml, nothing missing
        3. Two users in roles.yml, none in snowflake, return nothing
    """
    assert compare_users(users_roles_yaml, users_snowflake) == missing_users_in_roles


@pytest.mark.parametrize(
    "users_and_roles, expected_outcome, error_message",
    [
        (
            KEY_ROLES + ["some_role"] * ROLE_MULTIPLER,
            "success",
            None,
        ),  # 1. valid: includes KEY_ROLES and expected len
        (
            KEY_ROLES + ["some_role"],
            "failure",
            "Length of users_and_roles suspiciously small",
        ),  # 2. fails: has key roles, but not expected len
        (
            ["some_role"] * ROLE_MULTIPLER,
            "failure",
            "Not all key roles were found in roles.yml",
        ),  # 3. fails: no key roles, has expected len
    ],
)
def test_validate_users_and_roles(users_and_roles, expected_outcome, error_message):
    """Test validate_users_and_roles with various scenarios."""
    if expected_outcome == "success":
        # Should not raise any exception
        validate_users_and_roles(users_and_roles)
    else:
        # Should raise an AssertionError with the specific error message
        with pytest.raises(AssertionError, match=error_message):
            validate_users_and_roles(users_and_roles)
