"""
 This is main code base to kick of the ticket and ticket_audit transformation.
"""

import os
import sys
from logging import basicConfig, getLogger, info

import fire
from zip_sandbox_agreements_extract import zip_sandbox_agreements_extract
from zip_sandbox_requests_and_vendors_extract import zip_sandbox_requests_extract
from zip_sandbox_requests_and_vendors_extract import zip_sandbox_vendors_extract

config_dict = os.environ.copy()

if __name__ == "__main__":
    basicConfig(stream=sys.stdout, level=20)
    getLogger("snowflake.connector.cursor").disabled = True
    fire.Fire(
        {
            "vendors": zip_sandbox_vendors_extract,
            "requests": zip_sandbox_requests_extract,
            "agreements": zip_sandbox_agreements_extract,
        }
    )
    info("Complete.")
