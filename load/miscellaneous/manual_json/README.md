# Manual .json

This was used to upload manually generated `.json` files for this issue [#7345](https://handbook.gitlab.com/handbook/enterprise-data/platform/infrastructure/#airflow)

It was run inside of the `data-image` and I had to export the LOADER credentials manually.
