"""
Extracts data from the MailGun API event stream
"""
import datetime
import json
import sys
import time
from email import utils
from logging import basicConfig, error, getLogger, info
from os import environ as env
from typing import Dict, Iterator, List

import requests
from dateutil import parser as date_parser
from fire import Fire
from gitlabdata.orchestration_utils import (
    snowflake_engine_factory,
    snowflake_stage_load_copy_remove,
)
from mailgun_utils import Config, JsonUtils
from requests.exceptions import SSLError

config_dict = env.copy()
mailgun_config = Config.from_env()
json_utils = JsonUtils()


def get_mailgun_chunk_size(seq: List, size: int) -> Iterator[List]:
    """
    Splits a list into chunks of a given size.
    :param seq:
    :param size:
    :return:
    """
    return (seq[pos : pos + size] for pos in range(0, len(seq), size))


def get_mailgun_events(
    domain: str, event: str, formatted_start_date: str, formatted_end_date: str
) -> requests.Response:
    """
    Small convenience wrapper function for mailgun event requests,
    :param domain:
    :param event:
    :param formatted_start_date:
    :param formatted_end_date:
    :return:
    """
    return requests.get(
        url=f"{mailgun_config.base_url}/{domain}/events",
        auth=("api", str(mailgun_config.api_key)),
        timeout=mailgun_config.request_timeout,
        params={
            "begin": formatted_start_date,
            "end": formatted_end_date,
            "ascending": "yes",
            "event": event,
        },
    )


def process_response(response: requests.Response) -> tuple:
    """
    Process API response and extract items and next page token

    Args:
        response: API response object
    Returns:
        Tuple containing list of items and next page token
    """
    try:
        data = response.json()
    except json.decoder.JSONDecodeError:
        error("No response received")
        return [], None

    items = data.get("items", [])
    page_token = data.get("paging", {}).get("next")

    return items, page_token


def handle_empty_response(page_token: str) -> tuple:
    """
    Handle empty response by retrying once

    Args:
        page_token: Token for the page to retry
    Returns:
        Tuple containing list of items and next page token
    """
    info("Empty response received, retrying")
    time.sleep(60)
    response = requests.get(
        page_token,
        auth=("api", mailgun_config.api_key),
        timeout=mailgun_config.request_timeout,
    )
    return process_response(response)


def get_paginated_response(page_token: str) -> requests.Response:
    """
    Get response for a page token with SSL error handling

    Args:
        page_token: Token for the page to fetch
    Returns:
        API response
    """
    try:
        return requests.get(
            page_token,
            auth=("api", mailgun_config.api_key),
            timeout=mailgun_config.request_timeout,
        )
    except SSLError:
        error("SSL error received, waiting 30 seconds before retrying")
        time.sleep(30)
        return requests.get(
            page_token,
            auth=("api", mailgun_config.api_key),
            timeout=mailgun_config.request_timeout,
        )


def prepare_data(data: List[Dict]) -> List[Dict]:
    """
    Prepare data for loading
    :param data:
    :return:
    """
    flatten_data = [json_utils.flatten_json(x) for x in data]
    filtered_columns_data = [json_utils.filter_columns(x) for x in flatten_data]
    filtered_rows_data = [
        x for x in filtered_columns_data if not json_utils.filter_rows_per_subject(x)
    ]

    return filtered_rows_data


def extract_mailgun_events(
    event: str, start_date: datetime.datetime, end_date: datetime.datetime
) -> List[Dict]:
    """
    Requests and retrieves the event logs for a particular event.

    Args:
        event: Type of event to fetch
        start_date: Start date for fetching logs
        end_date: End date for fetching logs
    Returns:
        List of event log dictionaries
    """
    page_token = None
    all_results: List[Dict] = []

    formatted_start_date = utils.format_datetime(start_date)
    formatted_end_date = utils.format_datetime(end_date)

    for domain in mailgun_config.domains:
        while True:
            if page_token:
                response = get_paginated_response(page_token)
            else:
                response = get_mailgun_events(
                    domain, event, formatted_start_date, formatted_end_date
                )

            items, page_token = process_response(response)

            if not items:
                if page_token:
                    items, page_token = handle_empty_response(page_token)
                    if not items:
                        break
                else:
                    break

            if items:
                first_timestamp = items[0].get("timestamp")
                str_stamp = datetime.datetime.fromtimestamp(first_timestamp).strftime(
                    "%d-%m-%Y %H:%M:%S.%f"
                )
                info(f"Processed data starting on {str_stamp}")

                enriched_items = [
                    json_utils.enrich_json(x, ("domain", domain)) for x in items
                ]
                all_results.extend(enriched_items)

            if not page_token:
                break

    prepared_data = prepare_data(data=all_results)

    return prepared_data


def check_response(
    mailgun_domains: list,
    event: str,
    start_date: datetime.datetime,
    end_date: datetime.datetime,
) -> bool:
    """
    Validates API response status for each domain in the mailgun domains list.

    Args:
        mailgun_domains: List of mailgun domains to check
        event: Type of event to fetch
        start_date: Start date for fetching logs
        end_date: End date for fetching logs

    Returns:
        bool: True if there was an API error, False if all responses were successful
    """
    formatted_start_date = utils.format_datetime(start_date)
    formatted_end_date = utils.format_datetime(end_date)

    for domain in mailgun_domains:
        response = get_mailgun_events(
            domain=domain,
            event=event,
            formatted_start_date=formatted_start_date,
            formatted_end_date=formatted_end_date,
        )

        info(f"Response Status for domain {domain}: {response.status_code}")

        if response.status_code != 200:
            error(
                f"Error getting logs for domain {domain}, response {response.status_code} received"
            )
            return True

    return False


def upload_to_snowflake(file: str, event: str) -> None:
    """
    Upload data to Snowflake
    """
    snowflake_engine = snowflake_engine_factory(config_dict, "LOADER")

    snowflake_stage_load_copy_remove(
        file=file,
        stage=f"{mailgun_config.schema_name}.mailgun_load_{event}",
        table_path=f"{mailgun_config.schema_name}.{mailgun_config.table_name}_{event}",
        engine=snowflake_engine,
        on_error="ABORT_STATEMENT",
    )


def prepare_upload_file(content: list, event_to_upload: str) -> None:
    """
    Prepare file for upload and upload it to Snowflake
    :param content:
    :return:
    """
    # Stay under snowflakes max column size.
    for file_count, group in enumerate(get_mailgun_chunk_size(content, 5000), start=1):
        file_name = f"{event_to_upload}_{file_count}.json"

        with open(file_name, mode="w", encoding=mailgun_config.encoding) as outfile:
            json.dump(group, outfile)

        upload_to_snowflake(file=file_name, event=event_to_upload)


def load_event_logs(event: str, full_refresh: bool = False):
    """
    CLI main function, starting point for setting up engine and processing data.
    :param event:
    :param full_refresh:
    """

    if full_refresh:
        start_date = datetime.datetime(2025, 1, 1)
        end_date = datetime.datetime.now()
    else:
        # This extends the time window to handle late processing on the API.
        start_date = date_parser.parse(config_dict["START_TIME"]) - datetime.timedelta(
            hours=mailgun_config.overlapping_hours
        )
        end_date = start_date + datetime.timedelta(hours=mailgun_config.backfill_hours)

    info(
        f"Running from {start_date.strftime('%Y-%m-%dT%H:%M:%S%z')} to {end_date.strftime('%Y-%m-%dT%H:%M:%S%z')}"
    )

    is_bad_request = check_response(mailgun_config.domains, event, start_date, end_date)

    if is_bad_request:
        sys.exit(1)

    else:
        info("Extracting logs")

        results = extract_mailgun_events(event, start_date, end_date)

        info(f"Results length: {len(results)}")

        prepare_upload_file(content=results, event_to_upload=event)


if __name__ == "__main__":
    info("Start.")
    basicConfig(stream=sys.stdout, level=20)
    getLogger("snowflake.connector.cursor").disabled = True
    Fire({"load_event_logs": load_event_logs})
    info("Complete.")
