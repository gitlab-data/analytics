"""
Test unit for MailGun
"""
import json
import os
from unittest import mock
from unittest.mock import patch

import pytest
import requests
from extract.mailgun.src.execute import (
    prepare_data,
    get_mailgun_events,
    get_mailgun_chunk_size,
    mailgun_config,
    prepare_upload_file,
)
from extract.mailgun.src.mailgun_utils import Config


@pytest.fixture(autouse=True, name="set_env_variables")
def mock_settings_env_vars():
    """
    Simulate OS env. variables
    """
    with mock.patch.dict(os.environ, {"START_TIME": "2023-01-01T00:00:00Z"}):
        yield


@pytest.mark.parametrize(
    "input_list, chunk_size, expected_chunks",
    [
        # Test with a list of 10 items and chunk size of 3
        (list(range(10)), 3, [[0, 1, 2], [3, 4, 5], [6, 7, 8], [9]]),
        # Test with an empty list
        ([], 3, []),
        # Test with chunk size larger than list length
        (list(range(10)), 15, [list(range(10))]),
        # Test with chunk size equal to list length
        (list(range(10)), 10, [list(range(10))]),
    ],
)
def test_get_mailgun_chunk_size(input_list, chunk_size, expected_chunks):
    """
    Test the get_mailgun_chunk_size function with various input scenarios:
    - Normal case with multiple chunks
    - Empty input list
    - Chunk size larger than list length
    - Chunk size equal to list length
    """
    chunks = list(get_mailgun_chunk_size(input_list, chunk_size))
    assert chunks == expected_chunks


@patch("requests.get")
def test_get_mailgun_events(mock_get):
    """
    Test the get_logs function with mocked API response
    """
    # Test data setup
    test_data = {
        "items": [
            {"event": "delivered", "timestamp": 1234567890},
            {"event": "opened", "timestamp": 1234567891},
        ],
        "paging": {
            "next": "https://api.mailgun.net/v3/domain.com/events?page=next_token"
        },
    }

    # Mock response setup
    fake_response = requests.Response()
    fake_response.status_code = 200
    fake_response._content = json.dumps(test_data).encode("utf-8")
    mock_get.return_value = fake_response

    # Test parameters
    test_params = {
        "domain": "test.com",
        "event": "delivered",
        "formatted_start_date": "Wed, 01 Jan 2020 00:00:00 GMT",
        "formatted_end_date": "Thu, 02 Jan 2020 00:00:00 GMT",
    }

    # Execute function
    response = get_mailgun_events(**test_params)
    response_data = response.json()

    # Verify response structure and content
    assert response.status_code == 200
    assert isinstance(response_data, dict)
    assert all(key in response_data for key in ["items", "paging"])
    assert len(response_data["items"]) == len(test_data["items"])

    # Verify API call parameters
    config = Config.from_env()
    expected_params = {
        "url": f"{config.base_url}/{test_params['domain']}/events",
        "auth": ("api", str(config.api_key)),
        "timeout": config.request_timeout,
        "params": {
            "begin": test_params["formatted_start_date"],
            "end": test_params["formatted_end_date"],
            "ascending": "yes",
            "event": test_params["event"],
        },
    }

    mock_get.assert_called_once_with(**expected_params)


@pytest.mark.parametrize(
    "input_data, expected_output",
    [
        (
            [
                {"event": "delivered", "recipient": {"email": "user@example.com"}},
                {"event": "opened", "message": {"headers": {"subject": "Test"}}},
            ],
            [{"event": "delivered"}, {"event": "opened"}],
        ),
        (
            [
                {"event": "clicked", "url": "https://example.com", "ip": "192.168.1.1"},
                {
                    "event": "complained",
                    "domain": "example.com",
                    "timestamp": 1234567890,
                },
            ],
            [
                {"event": "clicked"},
                {
                    "event": "complained",
                    "domain": "example.com",
                    "timestamp": 1234567890,
                },
            ],
        ),
        ([], []),  # Test with empty input
    ],
)
def test_prepare_data(input_data, expected_output):
    """
    Test the prepare_data function with various input scenarios:
    - Normal case with nested structures
    - Case with flat structures
    - Empty input
    """
    result = prepare_data(input_data)

    assert result == expected_output

    # Verify that the output is a list of dictionaries
    assert isinstance(result, list)
    assert all(isinstance(item, dict) for item in result)

    # Verify that all nested structures are flattened
    for item in result:
        assert all(not isinstance(value, (dict, list)) for value in item.values())


# create test case for funciton prepare_upload_file
@patch("json.dump")
@patch("builtins.open")
@patch("extract.mailgun.src.execute.upload_to_snowflake")
def test_prepare_upload_file(mock_upload, mock_open, mock_dump):
    """
    Test the prepare_upload_file function with various scenarios:
    - Test with multiple chunks of data
    - Verify file operations
    - Verify upload calls
    """
    # Test data
    test_data = [
        {"event": "delivered", "id": "1"},
        {"event": "opened", "id": "2"},
        {"event": "clicked", "id": "3"},
    ]
    test_event = "test_event"

    # Execute function
    prepare_upload_file(content=test_data, event_to_upload=test_event)

    # Verify file operations
    mock_open.assert_called_once_with(
        f"{test_event}_1.json", mode="w", encoding=mailgun_config.encoding
    )

    # Verify json dump was called with correct data
    mock_dump.assert_called_once_with(test_data, mock_open().__enter__())

    # Verify upload was called with correct parameters
    mock_upload.assert_called_once_with(
        file=f"{test_event}_1.json",
        event=test_event,
    )

    # Test with empty data
    mock_open.reset_mock()
    mock_dump.reset_mock()
    mock_upload.reset_mock()

    prepare_upload_file(content=[], event_to_upload=test_event)

    # Verify no operations were performed with empty data
    mock_open.assert_not_called()
    mock_dump.assert_not_called()
    mock_upload.assert_not_called()
