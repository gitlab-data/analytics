"""
saas_usage_ping.py is responsible for orchestrating:
- usage ping combined metrics (sql + redis)
- usage ping namespace
"""

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow_utils import (
    generate_namespace_task,
    gitlab_pod_env_vars,
    service_ping_dag_config,
    service_ping_default_args,
    service_ping_default_pod_env_vars,
)

metrics_type = "database_namespace_metrics"
config = service_ping_dag_config[metrics_type]
NUMBER_OF_TASKS = config["NUMBER_OF_TASKS"]
pod_env_vars = {**gitlab_pod_env_vars, **service_ping_default_pod_env_vars}

dag = DAG(
    config["DAG_NAME"],
    default_args=service_ping_default_args,
    concurrency=5,
    description=config["DAG_DESCRIPTION"],
    schedule_interval="0 7 * * 1",
    catchup=False,
)

dummy_start = DummyOperator(task_id=f"start_{config['task_name']}", dag=dag)

for i in range(1, NUMBER_OF_TASKS + 1):
    dummy_start >> generate_namespace_task(
        dag=dag,
        metrics_type=metrics_type,
        current_chunk=i,
        number_of_tasks=NUMBER_OF_TASKS,
    )
