"""
Extract Zip data from the Zip API.
"""

import os
from logging import info
from typing import Dict, Union

from zip_sandbox_utils import (
    get_start_and_end_date,
    paginated_api_call,
    upload_events_to_snowflake,
)

config_dict = os.environ.copy()

PAGE_SIZE = 100


def get_zip_agreements(table_name: str, vendors_list: list) -> list:
    """
    Get agreements from the Zip API.
    """
    info(f"Extracting agreements for {len(vendors_list)} vendors")
    agreements_list = []
    for vendor_id in vendors_list:
        params = {"vendor_id": vendor_id, "page_size": PAGE_SIZE}
        agreements_list.extend(paginated_api_call(params, table_name))

    return agreements_list


def get_zip_vendor_id_from_requests(
    start_epoch: int, end_epoch: int, table_name: str
) -> list:

    """
    Get vendor information from the Zip API using request IDs.
    """
    info("Extracting the list of vendors from the requests")
    params: Dict[str, Union[int, str]] = {
        "last_updated_after": start_epoch,
        "last_updated_before": end_epoch,
        "page_size": PAGE_SIZE,
    }

    responses = paginated_api_call(params, table_name)
    return [
        response["vendor"]["id"]
        for response in responses
        if response["vendor"] is not None
    ]


def zip_sandbox_agreements_extract() -> None:
    """Main function."""
    table_name = "agreements"
    # set start date and end date
    start_epoch, end_epoch = get_start_and_end_date()
    # get vendor ids from requests
    vendors_list = get_zip_vendor_id_from_requests(
        start_epoch=start_epoch, end_epoch=end_epoch, table_name="requests"
    )
    # get agreements from endpoint
    extract_list = get_zip_agreements(table_name, vendors_list)
    # upload payload to snowflake
    upload_events_to_snowflake(extract_list, table_name)
