```sql
use role loader;
use database RAW;

create schema zip_sandbox;

CREATE STAGE RAW.zip_sandbox.zip_sandbox_load_stage
FILE_FORMAT = (TYPE = 'JSON');

create or replace table RAW.zip_sandbox.requests  (
  jsontext variant,
  uploaded_at timestamp_ntz(9) default CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9))
);
create or replace table RAW.zip_sandbox.agreements  (
  jsontext variant,
  uploaded_at timestamp_ntz(9) default CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9))
);
create or replace table RAW.zip_sandbox.vendors  (
  jsontext variant,
  uploaded_at timestamp_ntz(9) default CAST(CURRENT_TIMESTAMP() AS TIMESTAMP_NTZ(9))
);
```