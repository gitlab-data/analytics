""" Airflow DAG for removing duplicate data in gitlab.com tables"""

from datetime import datetime, timedelta
import logging
from typing import Any, Dict
from yaml import safe_load, YAMLError
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow_utils import (
    DATA_IMAGE,
    clone_and_setup_extraction_cmd,
    gitlab_defaults,
    slack_failed_task,
    gitlab_pod_env_vars,
    REPO_BASE_PATH,
)
from kube_secrets import (
    SNOWFLAKE_ACCOUNT,
    SNOWFLAKE_LOAD_PASSWORD,
    SNOWFLAKE_LOAD_ROLE,
    SNOWFLAKE_LOAD_USER,
    SNOWFLAKE_LOAD_WAREHOUSE,
)
from kubernetes_helpers import get_affinity, get_toleration

# Load the env vars into a dict and set Secrets
pod_env_vars = {**gitlab_pod_env_vars, **{}}

# List of tables to be excluded from deduplication
tables_to_exclude = [
    "ci_builds",
    "merge_request_diff_commits",
    "ci_job_artifacts",
    "ci_pipelines",
    "ci_builds_metadata",
]

# Default arguments for the DAG
default_args = {
    "depends_on_past": False,
    "on_failure_callback": slack_failed_task,
    "owner": "airflow",
    "retries": 0,
    "retry_delay": timedelta(minutes=1),
    "sla": timedelta(hours=24),
    "sla_miss_callback": slack_failed_task,
    "start_date": datetime(2023, 2, 21),
    "dagrun_timeout": timedelta(hours=6),
}

# Dictionary containing configuration for deduplication dags for customer DB and gitab.com
config_dict: Dict[str, Dict[str, Any]] = {
    "gitlab_com": {
        "full_path": f"{REPO_BASE_PATH}/extract/gitlab_saas_postgres_pipeline/manifests/el_gitlab_dotcom_scd_db_manifest.yaml",
        "task_name": "t_deduplication_gitlab_db",
        "dag_name": "t_deduplication_gitlab_db_scd",
        "dag_description": "This DAG removes duplicate data in gitlab.com tables in RAW layer",
        "snowflake_warehouse": "LOADING_M",
        "database_type": "gitlab_com",
    },
    "customer_db": {
        "full_path": f"{REPO_BASE_PATH}/extract/gitlab_saas_postgres_pipeline/manifests/el_saas_customers_scd_db_manifest.yaml",
        "task_name": "t_deduplication_customer_db",
        "dag_name": "t_deduplication_customer_db",
        "dag_description": "This DAG removes duplicate data in customer database tables in RAW layer",
        "snowflake_warehouse": "LOADING",
        "database_type": "customer_db",
    },
    "gitlab_com_incremental": {
        "full_path": f"{REPO_BASE_PATH}/extract/gitlab_saas_postgres_pipeline/manifests/el_gitlab_dotcom_db_manifest.yaml",
        "task_name": "t_deduplication_gitlab_db_incremental",
        "dag_name": "t_deduplication_gitlab_com_incremental",
        "dag_description": "This DAG removes duplicate data in incremental table in gitlab.com tables in RAW layer",
        "snowflake_warehouse": "LOADING_M",
        "database_type": "gitlab_com",
    },
}


def get_yaml_file(path: str):
    """
    Get all the report name for which tasks for loading
    needs to be created
    """

    with open(file=path, mode="r", encoding="utf-8") as file:
        try:
            return safe_load(stream=file)
        except YAMLError as exc:
            logging.error(f"Issue with the yaml file: {exc}")
            raise


def extract_table_dict_from_manifest(manifest_contents):
    """Extract table from the manifest file for which extraction needs to be done"""
    return manifest_contents["tables"] if manifest_contents.get("tables") else []


def create_deduplication_task(
    dag, config: Dict[str, Any], table: str
) -> KubernetesPodOperator:
    """Create a KubernetesPodOperator task for deduplication"""
    container_cmd_load = f"""
        {clone_and_setup_extraction_cmd} &&
        export SNOWFLAKE_LOAD_WAREHOUSE="{config['snowflake_warehouse']}" &&
        python3 gitlab_deduplication/main.py deduplication --table_name {table} --database_type {config['database_type']}
    """
    task_identifier = f"{config['task_name']}_{table.lower()}"

    return KubernetesPodOperator(
        **gitlab_defaults,
        image=DATA_IMAGE,
        task_id=task_identifier,
        name=task_identifier,
        pool="default_pool",
        secrets=[
            SNOWFLAKE_ACCOUNT,
            SNOWFLAKE_LOAD_ROLE,
            SNOWFLAKE_LOAD_USER,
            SNOWFLAKE_LOAD_WAREHOUSE,
            SNOWFLAKE_LOAD_PASSWORD,
        ],
        env_vars=pod_env_vars,
        affinity=get_affinity("extraction"),
        tolerations=get_toleration("extraction"),
        arguments=[container_cmd_load],
        dag=dag,
    )


def create_deduplication_dag(config: Dict[str, Any]) -> DAG:
    """Create a DAG for deduplication tasks"""
    dag = DAG(
        config["dag_name"],
        default_args=default_args,
        schedule_interval="0 11 * * 0",
        concurrency=3,
        catchup=False,
        description=config["dag_description"],
    )

    with dag:
        start = DummyOperator(task_id="Start")
        manifest_dict = get_yaml_file(path=config["full_path"])
        table_list = extract_table_dict_from_manifest(manifest_dict)

        for table in table_list:
            # check if the table is not from the list of table to be excluded.
            if table not in tables_to_exclude:
                deduplication_task = create_deduplication_task(dag, config, table)
                start >> deduplication_task

    return dag


# Create DAGs for each source
for source_name, config in config_dict.items():
    dag = create_deduplication_dag(config)
    globals()[config["dag_name"]] = dag
