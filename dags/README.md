# (Airflow) DAGs

The module contains all routines for managing Airflow DAGs and tasks. Detailed setup is exposed in 
[Airflow handbook page](https://handbook.gitlab.com/handbook/enterprise-data/platform/infrastructure/#airflow) 

