""" This file contains common operators/functions to be used across multiple DAGs """

import os
import urllib.parse
from datetime import date, datetime, timedelta
from typing import Dict, List

from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow.models import Variable
from airflow.providers.slack.operators.slack import SlackAPIPostOperator
from airflow.providers.slack.operators.slack_webhook import SlackWebhookOperator
from kube_secrets import (
    GITLAB_ANALYTICS_PRIVATE_TOKEN,
    SNOWFLAKE_ACCOUNT,
    SNOWFLAKE_LOAD_PASSWORD,
    SNOWFLAKE_LOAD_ROLE,
    SNOWFLAKE_LOAD_USER,
    SNOWFLAKE_PASSWORD,
    SNOWFLAKE_USER,
)
from kubernetes.client import models as k8s
from kubernetes_helpers import get_affinity, get_toleration

if os.environ["IN_CLOUD"] == "False":
    REPO_BASE_PATH = f"{os.environ['AIRFLOW_HOME']}/analytics"
else:
    REPO_BASE_PATH = f"{os.environ['AIRFLOW_HOME']}/dags/repo"

SSH_REPO = "git@gitlab.com:gitlab-data/analytics.git"
HTTP_REPO = "https://gitlab.com/gitlab-data/analytics.git"
DATA_IMAGE = "registry.gitlab.com/gitlab-data/data-image/data-image:v1.0.31"
DATA_IMAGE_3_10 = "registry.gitlab.com/gitlab-data/data-image/data-image:v2.0.12"
DBT_IMAGE = "registry.gitlab.com/gitlab-data/dbt-image:v0.0.9"
PERMIFROST_IMAGE = "registry.gitlab.com/gitlab-data/permifrost:v0.15.4"


analytics_pipelines_dag = [
    "dbt",
    "dbt_manual_refresh",
    "dbt_full_refresh_weekly",
    "dbt_netsuite_actuals_income_cogs_opex",
    "dbt_snowplow_backfill",
    "dbt_snowplow_backfill_specific_model",
    "dbt_snowplow_full_refresh",
    "t_prep_dotcom_usage_events_backfill",
    "dbt_sfdc_validation_and_run",
]


def split_date_parts(day: date, partition: str) -> Dict:
    if partition == "month":
        split_dict = {
            "year": day.strftime("%Y"),
            "month": day.strftime("%m"),
            "part": day.strftime("%Y_%m"),
        }

    return split_dict


def partitions(from_date: date, to_date: date, partition: str) -> List[dict]:
    """
    A list of partitions to build.
    """

    delta = to_date - from_date
    all_parts = [
        split_date_parts((from_date + timedelta(days=i)), partition)
        for i in range(delta.days + 1)
    ]

    seen = set()
    parts = []
    # loops through every day and pulls out unique set of date parts
    for p in all_parts:
        if p["part"] not in seen:
            seen.add(p["part"])
            parts.append({k: v for k, v in p.items()})
    return parts


class MultiSlackChannelOperator:
    """
    Class that enables sending notifications to multiple Slack channels
    """

    def __init__(self, channels, context):
        self.channels = channels
        self.context = context

    def execute(self):
        attachment, slack_channel, task_id, task_text = slack_defaults(
            self.context, "failure"
        )
        for c in self.channels:
            slack_alert = SlackAPIPostOperator(
                attachments=attachment,
                channel=c,
                task_id=task_id,
                text=task_text,
                token=os.environ["SLACK_API_TOKEN"],
                username="Airflow",
            )

            slack_alert.execute()


def slack_defaults(context, task_type):
    """
    Function to handle switching between a task failure and success.
    """
    base_url = "http://34.83.216.69:443/"
    execution_date = context["ts"]
    dag_context = context["dag"]
    dag_name = dag_context.dag_id
    dag_id = context["dag"].dag_id
    task_name = context["task"].task_id
    task_id = context["task_instance"].task_id
    execution_date_value = context["execution_date"]
    execution_date_epoch = execution_date_value.strftime("%s")
    execution_date_pretty = execution_date_value.strftime(
        "%a, %b %d, %Y at %-I:%M %p UTC"
    )

    # Generate the link to the logs
    log_params = urllib.parse.urlencode(
        {"dag_id": dag_id, "task_id": task_id, "execution_date": execution_date}
    )
    log_link = f"{base_url}/log?{log_params}"
    log_link_markdown = f"<{log_link}|View Logs>"

    if task_type == "success":
        if task_name == "snowflake-password-reset":
            slack_channel = "#data-lounge"
        else:
            slack_channel = dag_context.params.get(
                "slack_channel_override", "#analytics-pipelines"
            )

        color = "#1aaa55"
        fallback = "An Airflow DAG has succeeded!"
        task_id = "slack_succeeded"
        task_text = "Task succeeded!"

    if task_type == "failure":
        if dag_id in analytics_pipelines_dag:
            slack_channel = "#analytics-pipelines"
        else:
            slack_channel = dag_context.params.get(
                "slack_channel_override", "#data-pipelines"
            )
        color = "#a62d19"
        fallback = "An Airflow DAG has failed!"
        task_id = "slack_failed"
        task_text = "Task failure!"

    attachment = [
        {
            "mrkdwn_in": ["title", "value"],
            "color": color,
            "fallback": fallback,
            "fields": [
                {"title": "DAG", "value": dag_name, "short": True},
                {"title": "Task", "value": task_name, "short": True},
                {"title": "Logs", "value": log_link_markdown, "short": True},
                {"title": "Timestamp", "value": execution_date_pretty, "short": True},
            ],
            "footer": "Airflow",
            "footer_icon": "https://airflow.gitlabdata.com/static/pin_100.png",
            "ts": execution_date_epoch,
        }
    ]
    return attachment, slack_channel, task_id, task_text


def slack_snapshot_failed_task(context):
    """
    Function to be used as a callable for on_failure_callback for dbt-snapshots
    Send a Slack alert to #analytics-pipelines
    """
    multi_channel_alert = MultiSlackChannelOperator(
        channels=["#analytics-pipelines"], context=context
    )

    return multi_channel_alert.execute()


def slack_webhook_conn(slack_channel):
    if slack_channel == "#analytics-pipelines":
        slack_webhook = Variable.get("AIRFLOW_VAR_ANALYTICS_PIPELINES")
    elif slack_channel == "#data-science-pipelines":
        slack_webhook = Variable.get("AIRFLOW_VAR_DATA_SCIENCE_PIPELINES")
    else:
        slack_webhook = Variable.get("AIRFLOW_VAR_DATA_PIPELINES")
    airflow_http_con_id = Variable.get("AIRFLOW_VAR_SLACK_CONNECTION")
    return airflow_http_con_id, slack_webhook


def slack_failed_task(context):
    """
    Function to be used as a callable for on_failure_callback.
    Send a Slack alert.
    """

    attachment, slack_channel, task_id, task_text = slack_defaults(context, "failure")
    airflow_http_con_id, slack_webhook = slack_webhook_conn(slack_channel)

    slack_alert = SlackWebhookOperator(
        attachments=attachment,
        channel=slack_channel,
        task_id=task_id,
        message=task_text,
        http_conn_id=airflow_http_con_id,
        webhook_token=slack_webhook,
        username="Airflow",
    )
    return slack_alert.execute(context=None)


def slack_succeeded_task(context):
    """
    Function to be used as a callable for on_success_callback.
    Send a Slack alert.
    """

    attachment, slack_channel, task_id, task_text = slack_defaults(context, "success")
    airflow_http_con_id, slack_webhook = slack_webhook_conn(slack_channel)

    slack_alert = SlackWebhookOperator(
        attachments=attachment,
        channel="#analytics-pipelines",
        task_id=task_id,
        message=task_text,
        http_conn_id=airflow_http_con_id,
        webhook_token=slack_webhook,
        username="Airflow",
    )
    return slack_alert.execute(context=None)


def number_of_dbt_threads_argument(number_of_threads):
    return f"--threads {number_of_threads}"


container_resources = k8s.V1ResourceRequirements(
    requests={"memory": "2Gi", "cpu": "1000m"},
    limits={"memory": "20Gi", "cpu": "2000m"},
)

# GitLab default settings for all DAGs
gitlab_defaults = dict(
    get_logs=True,
    image_pull_policy="Always",
    in_cluster=not os.environ["IN_CLUSTER"] == "False",
    is_delete_operator_pod=True,
    namespace=os.environ["NAMESPACE"],
    container_resources=container_resources,
    cmds=["/bin/bash", "-c"],
)

# GitLab default environment variables for worker pods
env = os.environ.copy()
GIT_BRANCH = env["GIT_BRANCH"]
gitlab_pod_env_vars = {
    "CI_PROJECT_DIR": "/analytics",
    "EXECUTION_DATE": "{{ next_execution_date }}",
    "SNOWFLAKE_PREPARATION_SCHEMA": "preparation",
    "SNOWFLAKE_SNAPSHOT_DATABASE": (
        "RAW" if GIT_BRANCH == "master" else f"{GIT_BRANCH.upper()}_RAW"
    ),
    "SNOWFLAKE_LOAD_DATABASE": (
        "RAW" if GIT_BRANCH == "master" else f"{GIT_BRANCH.upper()}_RAW"
    ),
    "SNOWFLAKE_PREP_DATABASE": (
        "PREP" if GIT_BRANCH == "master" else f"{GIT_BRANCH.upper()}_PREP"
    ),
    "SNOWFLAKE_PROD_DATABASE": (
        "PROD" if GIT_BRANCH == "master" else f"{GIT_BRANCH.upper()}_PROD"
    ),
    "DBT_RUNNER": (
        "{{ task_instance_key_str }}__{{ run_id }}__{{ task_instance.try_number }}"
        if GIT_BRANCH == "master"
        else f"{GIT_BRANCH.upper()}"
    ),
}

# git commands
data_test_ssh_key_cmd = """
    export DATA_TEST_BRANCH="main" &&
    export DATA_SIREN_BRANCH="master" &&
    mkdir ~/.ssh/ &&
    touch ~/.ssh/id_rsa && touch ~/.ssh/config &&
    echo "$GIT_DATA_TESTS_PRIVATE_KEY" > ~/.ssh/id_rsa && chmod 0400 ~/.ssh/id_rsa &&
    echo "$GIT_DATA_TESTS_CONFIG" > ~/.ssh/config"""

clone_repo_cmd = f"""
    {data_test_ssh_key_cmd} &&
    if [[ -z "$GIT_COMMIT" ]]; then
        export GIT_COMMIT="HEAD"
    fi
    if [[ -z "$GIT_DATA_TESTS_PRIVATE_KEY" ]]; then
        export REPO="{HTTP_REPO}";
        else
        export REPO="{SSH_REPO}";
    fi &&
    echo "git clone -b {GIT_BRANCH} --single-branch --depth 1 $REPO" &&
    git clone -b {GIT_BRANCH} --single-branch --depth 1 $REPO &&
    echo "checking out commit $GIT_COMMIT" &&
    cd analytics &&
    git checkout $GIT_COMMIT &&
    cd .."""

clone_repo_sha_cmd = f"""
    {data_test_ssh_key_cmd} &&
    mkdir analytics &&
    cd analytics &&
    git init &&
    git remote add origin {SSH_REPO} &&
    echo "Fetching commit $GIT_COMMIT" &&
    git fetch origin --quiet &&
    git checkout $GIT_COMMIT"""

# extract command
clone_and_setup_extraction_cmd = f"""
    {clone_repo_cmd} &&
    export PYTHONPATH="$CI_PROJECT_DIR/orchestration/:$PYTHONPATH" &&
    cd analytics/extract/"""

# dbt commands
clone_and_setup_dbt_cmd = f"""
    {clone_repo_sha_cmd} &&
    cd transform/snowflake-dbt/"""

dbt_install_deps_cmd = f"""
    {clone_and_setup_dbt_cmd} &&
    dbt deps --profiles-dir profile"""

dbt_install_deps_and_seed_cmd = f"""
    {dbt_install_deps_cmd} &&
    dbt seed --profiles-dir profile --target prod --full-refresh"""

clone_and_setup_dbt_nosha_cmd = f"""
    {clone_repo_cmd} &&
    cd analytics/transform/snowflake-dbt/"""

dbt_install_deps_nosha_cmd = f"""
    {clone_and_setup_dbt_nosha_cmd} &&
    dbt deps --profiles-dir profile"""

dbt_install_deps_and_seed_nosha_cmd = f"""
    {dbt_install_deps_nosha_cmd} &&
    dbt seed --profiles-dir profile --target prod --full-refresh"""

# command to exclude models (for test models) in dbt test command
run_command_test_exclude = "--exclude staging.gitlab_com edm_snapshot"

# Add common components for the Service ping DAGs
service_ping_default_args = {
    "depends_on_past": False,
    "on_failure_callback": slack_failed_task,
    "owner": "airflow",
    "retries": 0,
    "start_date": datetime(2020, 6, 7),
    "dagrun_timeout": timedelta(hours=8),
}

service_ping_dag_secrets = [
    SNOWFLAKE_ACCOUNT,
    SNOWFLAKE_LOAD_ROLE,
    SNOWFLAKE_LOAD_USER,
    SNOWFLAKE_LOAD_PASSWORD,
    SNOWFLAKE_PASSWORD,
    SNOWFLAKE_USER,
    GITLAB_ANALYTICS_PRIVATE_TOKEN,
]

service_ping_default_pod_env_vars = {
    "RUN_DATE": "{{ next_ds }}",
    "SNOWFLAKE_SYSADMIN_ROLE": "TRANSFORMER",
    "SNOWFLAKE_LOAD_WAREHOUSE": "USAGE_PING",
}

namespace_metrics_base_cmd = (
    "cd analytics/extract/saas_usage_ping/ && python3 namespace_metrics_calculation.py"
)

service_ping_dag_config = {
    "database_instance_combined_metrics": {
        "NUMBER_OF_TASKS": 30,
        "DAG_NAME": "service_ping_database_instance_combined_metrics",
        "DAG_DESCRIPTION": "This DAG calculates instance combined metrics",
        "task_name": "instance-combined-metrics",
        "affinity": "extraction",
        "toleration": "extraction",
    },
    "database_namespace_metrics": {
        "NUMBER_OF_TASKS": 30,
        "DAG_NAME": "service_ping_database_namespace_metrics",
        "DAG_DESCRIPTION": "This DAG calculates database_namespace_metrics",
        "task_name": "database-namespace-metrics-chunk",
        "affinity": "extraction_highmem",
        "toleration": "extraction_highmem",
    },
    "internal_events_namespace_metrics": {
        "NUMBER_OF_TASKS": 30,
        "DAG_NAME": "service_ping_internal_events_namespace_metrics",
        "DAG_DESCRIPTION": "This DAG run to calculate metrics: Internal events namespace metrics",
        "task_name": "internal-events-namespace-metrics-chunk",
        "affinity": "extraction_highmem",
        "toleration": "extraction_highmem",
    },
    "database_namespace_metrics_backfill": {
        "DAG_NAME": "service_ping_database_namespace_metrics_backfill",
        "DAG_DESCRIPTION": (
            "This DAG runs on demand to do a backfill "
            "for database namespace metrics. "
            "In order to have this DAG run properly, "
            "the variable NAMESPACE_BACKFILL_VAR should be filled"
        ),
        "task_name": "database-namespace-metrics-backfill",
        "run_command": f"{namespace_metrics_base_cmd} backfill",
        "affinity": "extraction_highmem",
        "toleration": "extraction_highmem",
    },
    "internal_events_namespace_metrics_backfill": {
        "DAG_NAME": "service_ping_internal_events_namespace_metrics_backfill",
        "DAG_DESCRIPTION": (
            "This DAG runs on demand to do a backfill "
            "for internal_events_namespace_metrics_backfill metrics. "
            "In order to have this DAG run properly, "
            "the variable NAMESPACE_BACKFILL_VAR should be filled"
        ),
        "task_name": "internal-events-namespace-metrics-backfill",
        "run_command": f"{namespace_metrics_base_cmd} backfill",
        "affinity": "extraction_highmem",
        "toleration": "extraction_highmem",
    },
}


def get_service_ping_task_name(
    metrics_type: str, current_chunk: int, number_of_tasks: int
) -> str:
    """
    Generate task name
    """

    return f"{service_ping_dag_config[metrics_type]['task_name']}-{current_chunk:02}-{number_of_tasks:02}"


def generate_service_ping_command(
    metrics_type: str, chunk_no: int, number_of_tasks: int
):
    """
    Generate command to run instance_namespace_metrics (per chunk)
    """
    full_run_command = f"{namespace_metrics_base_cmd} run --ping_date=$RUN_DATE --chunk_no={chunk_no} --number_of_tasks={number_of_tasks} --metrics_type={metrics_type}"
    return f"""{clone_repo_cmd} && {full_run_command}"""


def generate_namespace_task(
    dag, metrics_type: str, current_chunk: int, number_of_tasks: int
) -> KubernetesPodOperator:
    """
    Generate tasks for namespace
    """
    task_id = task_name = get_service_ping_task_name(
        metrics_type=metrics_type,
        current_chunk=current_chunk,
        number_of_tasks=number_of_tasks,
    )

    command = generate_service_ping_command(
        metrics_type=metrics_type,
        chunk_no=current_chunk,
        number_of_tasks=number_of_tasks,
    )

    pod_env_vars = {**gitlab_pod_env_vars, **service_ping_default_pod_env_vars}

    return KubernetesPodOperator(
        **gitlab_defaults,
        image=DATA_IMAGE_3_10,
        task_id=task_id,
        name=task_name,
        secrets=service_ping_dag_secrets,
        env_vars=pod_env_vars,
        arguments=[command],
        dag=dag,
        retries=3,
        affinity=get_affinity(service_ping_dag_config[metrics_type]["affinity"]),
        tolerations=get_toleration(service_ping_dag_config[metrics_type]["toleration"]),
    )


# Backfill routines
def get_backfill_command(metrics_type: str):
    """
    Namespace, Group, Project, User Level Usage Ping
    Generate execution command to call Python code
    """
    cmd = f"""
            {clone_repo_cmd} &&
            {service_ping_dag_config[metrics_type]["run_command"]} --ping_date=$RUN_DATE --metrics_to_backfill=$METRICS_BACKFILL --metrics_type={metrics_type}
        """
    return cmd


def date_to_str(input_date: date) -> str:
    """
    Convert date to string to assign it to DAG name
    """
    return input_date.strftime("%Y%m%d")


def get_backfill_task_name(metrics_type: str, run_date: date) -> str:
    """
    Generate task name
    """
    start_monday = date_to_str(input_date=run_date)
    return f"{service_ping_dag_config[metrics_type]['task_name']}-{start_monday}"


def get_run_dates(start: date, end: date) -> list:
    """
    Generate date range for loop to create tasks
    """
    res = []
    curr_date = start

    while curr_date <= end:
        res.append(curr_date.date())

        curr_date += timedelta(days=7)

    return res


def get_monday(day: datetime):
    """
    Get Monday from the input day
    """
    res = day - timedelta(days=day.weekday())

    return res


def get_date(parameters, date_param: str) -> datetime:
    """
    Get starting date (Monday in this case)
    Convert date str to date type,
    used when user passes in airflow backfill date params as string
    """
    res = parameters.get(date_param)

    return datetime.strptime(res, "%Y-%m-%d")


def generate_backfill_task(dag, metrics_type: str, metrics, run_date: date) -> None:
    """
    Generate tasks for back-filling DAG start from Monday,
    as the original pipeline run on Monday
    """

    pod_env_vars = service_ping_default_pod_env_vars
    # override default parameters
    pod_env_vars["RUN_DATE"] = run_date.isoformat()
    pod_env_vars["METRICS_BACKFILL"] = metrics

    pod_env_vars = {**gitlab_pod_env_vars, **pod_env_vars}

    task_id = task_name = get_backfill_task_name(
        metrics_type=metrics_type, run_date=run_date
    )

    command = get_backfill_command(metrics_type=metrics_type)

    return KubernetesPodOperator(
        **gitlab_defaults,
        image=DATA_IMAGE_3_10,
        task_id=task_id,
        name=task_name,
        secrets=service_ping_dag_secrets,
        env_vars=pod_env_vars,
        arguments=[command],
        affinity=get_affinity("extraction_highmem"),
        tolerations=get_toleration("extraction_highmem"),
        dag=dag,
    )
