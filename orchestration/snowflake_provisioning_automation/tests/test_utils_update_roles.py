import requests
import pytest
from utils_update_roles import get_roles_from_url


@pytest.mark.parametrize(
    "branch, expected_outcome",
    [
        ("master", "success"),
        ("!@#$%^&()", "failure"),
    ],
)
def test_get_roles_from_url(branch, expected_outcome):
    """
    Test 1: when downloading roles.yml from master br, len(roles) > 200
    Test 2: when downloading from bad url, function should return 404 error
    """
    if expected_outcome == "success":
        roles_data = get_roles_from_url(branch)
        assert len(roles_data["roles"]) > 200
    else:
        with pytest.raises(
            requests.exceptions.HTTPError, match="404 Client Error: Not Found.*"
        ):
            get_roles_from_url(branch)
