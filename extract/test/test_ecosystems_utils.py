"""
Test the EcoSystemsUtils class
"""

from unittest import mock

import pytest
from extract.ecosystems.src.ecosystems_utils import Config, JsonUtils


@pytest.mark.parametrize(
    "attribute,expected_value",
    [
        ("base_url", "https://doclib.ecosystems.us/api/v1/"),
        ("endpoints", (list, 2)),  # (type, length)
        ("encoding", "utf-8"),
        ("schema_name", "ecosystems"),
        ("backfill_hours", 24),
        ("overlapping_hours", 2),
    ],
)
def test_config_from_env(attribute, expected_value):
    """Test Config() method attributes"""
    config = Config()

    assert isinstance(config, Config)

    if isinstance(expected_value, tuple):
        # Handle special case for endpoints list
        expected_type, expected_length = expected_value
        assert isinstance(getattr(config, attribute), expected_type)
        assert len(getattr(config, attribute)) == expected_length
    else:
        # Handle regular attribute assertions
        assert getattr(config, attribute) == expected_value


@pytest.mark.parametrize(
    "expected_header",
    [
        "Authorization",
        # Add other expected headers if needed
    ],
)
def test_json_utils_init(expected_header):
    """Test JsonUtils initialization

    Args:
        expected_header (str): Header key that should exist in JsonUtils headers
    """
    json_utils = JsonUtils()

    # Test instance type
    assert isinstance(json_utils, JsonUtils)

    # Test headers existence
    assert (
        expected_header in json_utils.headers
    ), f"Header '{expected_header}' not found in JsonUtils headers"


@pytest.mark.parametrize(
    "response_text,expected_result",
    [
        ('{"key": "value"}', {"key": "value"}),  # Simple key-value pair
        ("[]", []),  # Empty array
        (
            '{"nested": {"key": "value"}}',
            {"nested": {"key": "value"}},
        ),  # Nested structure
        ('{"numbers": [1, 2, 3]}', {"numbers": [1, 2, 3]}),  # Array within object
    ],
)
def test_convert_response_to_json(response_text, expected_result):
    """Test JsonUtils.convert_response_to_json method with various JSON structures

    Args:
        response_text (str): Mock response text containing JSON string
        expected_result (dict/list): Expected parsed JSON result

    Tests:
        - Verify correct parsing of simple JSON objects
        - Verify correct parsing of empty arrays
        - Verify correct parsing of nested structures
        - Verify correct parsing of mixed data types
    """
    # Setup mock response
    mock_response = mock.Mock(text=response_text)

    # Execute conversion
    result = JsonUtils.convert_response_to_json(mock_response)

    # Verify result
    assert result == expected_result, f"Failed to parse JSON: {response_text}"


@pytest.mark.parametrize(
    "invalid_response_text",
    [
        "invalid json",
        "{unclosed_object",
        "[1, 2,]",  # Invalid trailing comma
        "{'single_quotes': 'invalid'}",  # Single quotes not valid JSON
    ],
)
def test_convert_response_to_json_exception(invalid_response_text):
    """Test JsonUtils.convert_response_to_json method with invalid JSON inputs

    Args:
        invalid_response_text (str): Invalid JSON string that should raise an exception

    Tests:
        - Verify exception handling for completely invalid JSON
        - Verify exception handling for malformed JSON objects
        - Verify exception handling for invalid array syntax
        - Verify exception handling for incorrect quote usage
    """
    mock_response = mock.Mock(text=invalid_response_text)

    with pytest.raises(Exception):
        JsonUtils.convert_response_to_json(mock_response)
