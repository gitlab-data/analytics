"""
Extract Zip data from the Zip API.
"""

import os
from logging import info
from typing import Dict, Union

from zip_sandbox_utils import (
    get_start_and_end_date,
    paginated_api_call,
    upload_events_to_snowflake,
)

config_dict = os.environ.copy()

PAGE_SIZE = 100


def zip_common_vendors_and_requests(
    start_epoch: int, end_epoch: int, table_name: str
) -> list:
    """
    Get requests from the Zip API.
    """
    info(f"Extracting {table_name}")
    response_list = []
    params: Dict[str, Union[int, str]] = {
        "last_updated_after": start_epoch,
        "last_updated_before": end_epoch,
        "page_size": PAGE_SIZE,
    }
    response_list.extend(paginated_api_call(params, table_name))

    return response_list


def zip_sandbox_extract(table_name: str) -> None:
    """
    Generic extract function for ZIP sandbox data.

    Args:
        table_name: Name of the table to extract data for ("requests" or "vendors")
    """
    # Set start date and end date
    start_epoch, end_epoch = get_start_and_end_date()

    # Get data from endpoint
    extract_list = zip_common_vendors_and_requests(start_epoch, end_epoch, table_name)

    # Upload payload to snowflake
    upload_events_to_snowflake(extract_list, table_name)


def zip_sandbox_requests_extract() -> None:
    """Extract function for requests table"""
    zip_sandbox_extract("requests")


def zip_sandbox_vendors_extract() -> None:
    """Extract function for vendors table"""
    zip_sandbox_extract("vendors")
