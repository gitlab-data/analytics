#!/usr/bin/env python3
import logging
import json
import sys
from os import environ as env
from typing import Dict, List
import urllib

from fire import Fire
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine
from sqlalchemy.exc import ProgrammingError
from yaml import load, safe_load, YAMLError

from gitlabdata.orchestration_utils import query_executor, execute_query_str


# Set logging defaults
logging.basicConfig(stream=sys.stdout, level=20)  # set level=10 for DEBUG


class SnowflakeManager:
    def __init__(self, config_vars: Dict):
        self.engine = create_engine(
            URL(
                user=config_vars["SNOWFLAKE_USER"],
                password=config_vars["SNOWFLAKE_PASSWORD"],
                account=config_vars["SNOWFLAKE_ACCOUNT"],
                role=config_vars["SNOWFLAKE_SYSADMIN_ROLE"],
                warehouse=config_vars["SNOWFLAKE_LOAD_WAREHOUSE"],
            )
        )

        # Snowflake database name should be in CAPS
        # see https://gitlab.com/meltano/analytics/issues/491
        self.prep_database = "{}_PREP".format(config_vars["BRANCH_NAME"].upper())
        self.prod_database = "{}_PROD".format(config_vars["BRANCH_NAME"].upper())
        self.raw_database = "{}_RAW".format(config_vars["BRANCH_NAME"].upper())

    def generate_db_queries(
        self,
        database_name: str,
        cloned_database: str,
        optional_schema_to_clone: str,
        source_db: str,
    ) -> List[str]:
        """
        Generate the queries to clone and provide permissions for databases.
        """

        # Queries for database cloning and permissions
        check_db_exists_query = """use database "{0}";"""
        create_query = """create or replace database "{0}" {1};"""
        grant_query = """grant ownership on database "{0}" to TRANSFORMER;"""

        clone_schema_query = """create schema "{0}"."{1}" clone "{2}"."{1}"; """

        usage_roles = ["LOADER", "TRANSFORMER", "ENGINEER"]
        usage_grant_query_with_params = (
            """grant create schema, usage on database "{0}" to {1}"""
        )
        usage_grant_queries = [
            usage_grant_query_with_params.format(database_name, role)
            for role in usage_roles
        ]

        # The order of the queries matters!
        queries = [
            check_db_exists_query.format(database_name),
            create_query.format(database_name, cloned_database),
            grant_query.format(database_name),
        ] + usage_grant_queries

        if optional_schema_to_clone != "":
            create_schema = clone_schema_query.format(
                database_name.upper(),
                optional_schema_to_clone.upper(),
                source_db.upper(),
            )
            grant_on_schema_query_with_params = (
                """grant create table, usage on schema "{0}"."{1}" to "{2}";"""
            )
            schema_grants = [
                grant_on_schema_query_with_params.format(
                    database_name.upper(), optional_schema_to_clone.upper(), role
                )
                for role in usage_roles
            ]
            future_grant_on_tables_in_schema = """grant ALL PRIVILEGES on future tables in schema "{0}"."{1}" to role {2};"""
            schema_grants = schema_grants + [
                future_grant_on_tables_in_schema.format(
                    database_name.upper(), optional_schema_to_clone.upper(), role
                )
                for role in usage_roles
            ]
            queries = queries + [create_schema] + schema_grants

        return queries

    def manage_clones(
        self,
        database: str,
        empty: bool = False,
        force: bool = False,
        schema: str = "",
        include_stages: bool = False,
    ) -> None:
        """
        For the creation of zero copy clones in Snowflake.
        """

        if schema != "":
            empty = True

        databases = {
            "prep": self.prep_database,
            "prod": self.prod_database,
            "raw": self.raw_database,
        }

        create_db = databases[database]
        clone_db = f"clone {database}" if not empty else ""
        queries = self.generate_db_queries(create_db, clone_db, schema, database)

        db_exists = self.check_if_db_exists(create_db, force)

        # If the DB doesn't exist or --force is true, create or replace the db
        if not db_exists:
            logging.info("Creating or replacing DBs")
            for query in queries[1:]:
                try:
                    logging.info("Executing Query: {}".format(query))
                    connection = self.engine.connect()
                    [result] = execute_query_str(connection, query).fetchone()
                    logging.info("Query Result: {}".format(result))
                finally:
                    connection.close()
                    self.engine.dispose()

            if include_stages:
                self.clone_stages(create_db, database, schema)

        else:
            logging.info("DB exists, running clone queries")

            for query in queries[3:]:
                try:
                    logging.info("Executing Query: {}".format(query))
                    connection = self.engine.connect()
                    [result] = execute_query_str(connection, query).fetchone()
                    logging.info("Query Result: {}".format(result))
                finally:
                    connection.close()
                    self.engine.dispose()

            if include_stages:
                self.clone_stages(create_db, database, schema)

    @staticmethod
    def get_roles() -> list:
        """
        Retrieve Snowflake roles from roles.yml
        """

        roles_yaml_url = "https://gitlab.com/gitlab-data/analytics/-/raw/master/permissions/snowflake/roles.yml"  # needs to stay on master branch
        with urllib.request.urlopen(roles_yaml_url) as data:
            try:
                roles_yaml = safe_load(data)
            except YAMLError as exc:
                logging.info(f"yaml error: {exc}")
        roles = roles_yaml["roles"]

        return roles

    def get_upstream_models(self, file: str) -> list:
        """
        parse json for dbt models
        """
        modelsList = []
        upstream_objectsList = []
        existing_models = []
        with open(file) as f:

            for jsonObj in f:
                modelsDict = json.loads(jsonObj)
                modelsList.append(modelsDict)

        return modelsList

    def get_role_inheritances(self, role_name: str, roles_list: list) -> list:
        """
        Traverse list of dictionaries with snowflake roles to compile role inheritances
        """
        role_inheritances = next(
            (
                role[role_name].get("member_of", [])
                for role in roles_list
                if role.get(role_name)
            ),
            [],
        )
        return role_inheritances + [
            inherited
            for direct in role_inheritances
            for inherited in self.get_role_inheritances(direct, roles_list)
        ]

    def get_query_results(self, connection, query: str) -> list:
        """
        run snowflake query with existing connection with logging and parsing results to list
        """
        logging.debug(f"Executing: {query}=")
        results = connection.execute(query).fetchall()
        results_list = [row for t in results for row in t]
        logging.debug(f"{results_list}")

        return results_list

    def run_sql_cmd(self, connection, command: str) -> list:
        """
        run snowflake command with existing connection with logging
        """
        logging.debug(f"Executing: {command}=")
        results = connection.execute(command).fetchone()
        logging.debug(f"{results}=")

    def grant_clones(self, role: str, database: str, file: str):
        """
        Grant privileges on a clone based on existing grants in production databases or upstream dependencies for new objects.
        """

        if database == "prep":
            clone = self.prep_database
        elif database == "prod":
            clone = self.prod_database

        roles = self.get_roles()
        upstream_models = self.get_upstream_models(file)

        logging.info(f"running for role {role}")

        role = role.lower()
        inherited_roles = self.get_role_inheritances(role, roles)
        logging.debug(f"found inherited roles: {inherited_roles}")
        inherited_roles_in = "', '".join(inherited_roles)

        upstream_model_object_list = []
        for model in upstream_models:
            upstream_model_object_list.append(
                f"{model['config']['schema']}.{model['alias']}"
            )
        upstream_model_object_list_in = "', '".join(upstream_model_object_list)
        logging.info(f"Found upstream models: {upstream_model_object_list}")

        try:
            connection = self.engine.connect()

            logging.info("getting list of objects in clone")
            get_cloned_objects_query = f"""
                SELECT
                    table_schema ||'.'|| table_name
                FROM "{clone.upper()}".information_schema.tables
                where table_schema !='INFORMATION_SCHEMA'
                ;
            """
            cloned_objects = self.get_query_results(
                connection, get_cloned_objects_query
            )

            if len(cloned_objects) > 0:

                logging.info("getting list of exisitng objects for upstream models")
                get_upstream_objects_query = f"""
                    SELECT
                        table_schema ||'.'|| table_name
                    FROM "{database.upper()}".information_schema.tables
                    WHERE table_schema ||'.'|| table_name IN ('{upstream_model_object_list_in.upper()}')
                    ;
                """
                upstream_objects = self.get_query_results(
                    connection, get_upstream_objects_query
                )

                logging.info("getting grants on existing upstream objects")
                upstream_objects_in = "', '".join(upstream_objects)
                get_grants_on_upstream_objects_query = f"""
                    SELECT DISTINCT
                    table_schema || '.' || name AS schema_table_name
                    FROM snowflake.account_usage.grants_to_roles
                    WHERE table_catalog = '{database.upper()}'
                    AND privilege = 'SELECT'
                    AND table_schema ||'.'|| name IN ('{upstream_objects_in}')
                    AND (grantee_name = '{role.upper()}'
                    OR LOWER(grantee_name) in ('{inherited_roles_in}')
                    );
                """
                grants_on_upstream_objects = self.get_query_results(
                    connection, get_grants_on_upstream_objects_query
                )

                logging.info(f"granting usage on {clone}")

                grant_usage_on_db = f"""
                    GRANT USAGE ON DATABASE "{clone}" TO ROLE {role};
                """
                self.run_sql_cmd(connection, grant_usage_on_db)

                grant_usage_on_schema = f"""
                    GRANT USAGE ON ALL SCHEMAS IN DATABASE "{clone}" TO ROLE {role};
                """
                self.run_sql_cmd(connection, grant_usage_on_schema)

                logging.info("comparing upstream objects to existing grants")
                logging.info(
                    f"found {len(upstream_objects)} objects in {database}: {upstream_objects}"
                )
                logging.info(
                    f"found grants on {database} for: {grants_on_upstream_objects}"
                )

                if set(upstream_objects) == set(grants_on_upstream_objects):

                    cloned_objects_lower = [item.lower() for item in cloned_objects]
                    grantable_objects = set(cloned_objects_lower).intersection(
                        set(upstream_model_object_list)
                    )

                    for schema_table_name in grantable_objects:
                        fully_qualified_object_name = f'"{clone}".{schema_table_name}'
                        grant_select_on_object = f"""
                            GRANT SELECT ON {fully_qualified_object_name} TO ROLE {role}
                        """
                        logging.info(f"Running: {grant_select_on_object}")
                        self.run_sql_cmd(connection, grant_select_on_object)

                    logging.info(f"✅ Completed grants for {role} on {clone} ✅")

                else:

                    logging.info(
                        f"🚨 upstream objects in {clone} did not match upstream grants for {role} 🚨"
                    )

            else:
                logging.info(
                    f"🚨🚨 No objects found in clone: {clone}. Have you built the database objects for {clone} yet?🚨🚨"
                )

        finally:
            logging.info("done")
            connection.close()
            self.engine.dispose()

    ###
    def delete_clones(self):
        """
        Delete a clone.
        """
        db_list = [
            self.prep_database,
            self.prod_database,
            self.raw_database,
        ]

        for db in db_list:
            query = 'DROP DATABASE IF EXISTS "{}";'.format(db)
            try:
                logging.info("Executing Query: {}".format(query))
                connection = self.engine.connect()
                [result] = execute_query_str(connection, query).fetchone()
                logging.info("Query Result: {}".format(result))
            finally:
                connection.close()
                self.engine.dispose()

    def create_table_clone(
        self,
        source_database: str,
        source_schema: str,
        source_table: str,
        target_database: str,
        target_table: str,
        target_schema: str = None,
        timestamp: str = None,
    ):
        """
        Create a zero copy clone of a table (optionally at a given timestamp)
        source_database: database of table to be cloned
        source_schema: schema of table to be cloned
        source_table: name of table to cloned
        target_database: name of clone database
        target_table: name of clone table
        target_schema: schema of clone table
        timestamp: timestamp indicating time of clone in format yyyy-mm-dd hh:mi:ss
        """
        timestamp_format = """yyyy-mm-dd hh:mi:ss"""
        if not target_schema:
            target_schema = source_schema

        queries = []
        # Tries to create the schema its about to write to
        # If it does exists, {schema} already exists, statement succeeded.
        # is returned.
        schema_check = (
            f"""CREATE SCHEMA IF NOT EXISTS "{target_database}".{target_schema};"""
        )
        queries.append(schema_check)

        clone_sql = f"""create table if not exists {target_database}.{target_schema}.{target_table} clone "{source_database}".{source_schema}.{source_table}"""
        if timestamp and timestamp_format:
            clone_sql += f""" at (timestamp => to_timestamp_tz('{timestamp}', '{timestamp_format}'))"""
        clone_sql += " COPY GRANTS;"
        # Drop statement for safety
        queries.append(
            f"drop table if exists {target_database}.{target_schema}.{target_table};"
        )
        queries.append(clone_sql)
        connection = self.engine.connect()
        try:
            for q in queries:
                logging.info("Executing Query: {}".format(q))
                [result] = execute_query_str(connection, q).fetchone()
                logging.info("Query Result: {}".format(result))
        finally:
            connection.close()
            self.engine.dispose()

        return self

    def check_if_table_exists(self, database: str, schema: str, table_name: str):
        """
        Simple utility function written to check if a table exists in the database

        :param database:
        :param schema:
        :param table_name:
        :return:
        """
        table_query = f"""
                     select COUNT(*) as table_count
                     from "{database}".information_schema.tables
                     where table_schema = '{schema.upper()}'
                     and table_name = '{table_name.upper()}'
                     """

        table_result = query_executor(self.engine, table_query)
        if table_result[0]["table_count"] == 0:
            return False
        else:
            return True

    def clone_stages(self, create_db: str, database: str, schema: str = ""):
        """
         Clones the stages available in a DB or schema (if specified).
         Required as SnowFlake currently does not support the cloning of internal stages.
        :param create_db:
        :param database:
        :param schema:
        """
        stages_query = f"""
             SELECT
                 stage_schema,
                 stage_name,
                 stage_url,
                 stage_type
             FROM {database}.information_schema.stages
             {f"WHERE stage_schema = '{schema.upper()}'" if schema != "" else ""}
             """

        stages = query_executor(self.engine, stages_query)

        for stage in stages:
            output_stage_name = (
                f""" "{create_db}"."{stage['stage_schema']}"."{stage['stage_name']}" """
            )
            from_stage_name = f""" "{database.upper()}"."{stage['stage_schema']}"."{stage['stage_name']}" """

            if stage["stage_type"] == "External Named":
                clone_stage_query = f"""
                    CREATE OR REPLACE STAGE {output_stage_name} LIKE
                    {from_stage_name}
                    """

                grants_query = f"""
                    GRANT USAGE ON STAGE {output_stage_name} TO LOADER
                    """

            else:
                clone_stage_query = f"""
                    CREATE OR REPLACE STAGE {output_stage_name}
                    """

                grants_query = f"""
                    GRANT READ, WRITE ON STAGE {output_stage_name} TO LOADER
                    """

            try:
                logging.info(f"Creating stage {output_stage_name}")
                res = query_executor(self.engine, clone_stage_query)
                logging.info(res[0])

                logging.info("Granting rights on stage to LOADER")
                res = query_executor(self.engine, grants_query)
                logging.info(res[0])

            except ProgrammingError as prg:
                # Catches permissions errors
                logging.error(prg._sql_message(as_unicode=False))

    def check_if_db_exists(self, database, force):
        # if force is false, check if the database exists
        check_db_exists_query = f"""use database "{database}";"""
        if force:
            logging.info("Forcing a create or replace...")
            db_exists = False
        else:
            try:
                logging.info("Checking if DB exists...")
                connection = self.engine.connect()
                execute_query_str(connection, check_db_exists_query)
                logging.info("DBs exist...")
                db_exists = True
            except:
                logging.info("DB does not exist...")
                db_exists = False
            finally:
                connection.close()
                self.engine.dispose()
        return db_exists

    def clone_database_by_schemas(
        self,
        database: str,
        include_stages: bool = False,
    ):
        databases = {
            "prep": self.prep_database,
            "prod": self.prod_database,
            "raw": self.raw_database,
        }

        create_db = databases[database]

        db_exists = self.check_if_db_exists(
            database=create_db,
            force=False,
        )

        schema_query = f"""
                        SELECT DISTINCT table_schema AS table_schema
                        FROM {database}.INFORMATION_SCHEMA.TABLES
                        WHERE TABLE_SCHEMA NOT IN ('INFORMATION_SCHEMA', 'PUBLIC')
                        {' AND TABLE_SCHEMA NOT IN ( SELECT DISTINCT table_schema FROM "{create_db}".INFORMATION_SCHEMA.TABLES)'
                        if db_exists else ''}
                        """

        try:
            logging.info(f"Getting schemas {schema_query}")
            res = query_executor(self.engine, schema_query)
        except ProgrammingError as prg:
            # Catches permissions errors
            logging.error(prg._sql_message(as_unicode=False))

        usage_roles = ["LOADER", "TRANSFORMER", "ENGINEER"]

        for r in res:
            try:
                self.manage_clones(
                    database=database,
                    force=True,
                    schema=r["table_schema"],
                    include_stages=include_stages,
                )
            except Exception as exc:
                logging.info(f"{r['schema']} didn't work")
                logging.info(exc)


if __name__ == "__main__":
    snowflake_manager = SnowflakeManager(env.copy())
    Fire(snowflake_manager)
